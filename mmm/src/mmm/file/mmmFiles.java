/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.file;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javafx.scene.image.Image;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javax.json.JsonString;
import mmm.data.mmmData;
import mmm.data.DraggableStation;
import mmm.data.DraggableLineEnd;
import mmm.data.Draggable;
import static mmm.data.Draggable.LABEL;
import static mmm.data.Draggable.LINEEND;
import static mmm.data.Draggable.STATION;
import mmm.data.DraggableLabel;
import mmm.data.DraggableImageView;
import mmm.gui.mmmWorkspace;
/**
 *
 * @author shawn
 */
public class mmmFiles implements AppFileComponent{
    //everything for export
    static final String JSON_NAME = "name";
    
    static final String JSON_LINES = "lines";
    
    static final String JSON_CIRCULAR = "circular";
    static final String JSON_COLOR = "color";
    static final String JSON_RED = "red";
    static final String JSON_GREEN = "green";
    static final String JSON_BLUE = "blue";
    static final String JSON_ALPHA = "alpha";
    
    static final String JSON_LINE_STATIONS = "station_names";
    
    static final String JSON_MAP_STATIONS = "stations";
    static final String JSON_X = "x";
    static final String JSON_Y = "y";
    //construction of physical lines will be done via formLine while iterating through the stations arraylist
    //save
    //everything for canvas
    static final String JSON_BG_COLOR = "background_color";
    static final String JSON_BG_IMAGE_PATH = "background_image";
    static final String JSON_USING_IMAGE = "using_bg";
    //mmmData
    static final String JSON_NODES = "nodes";
    static final String JSON_TYPE = "type";
    //DraggableStation
    static final String JSON_STATION_ROTATION = "rotation";
    static final String JSON_STATION_POSITION = "position";
    static final String JSON_RADIUS = "radius";
    static final String JSON_FILL_COLOR = "fill_color";
    //DraggableLineEnd
    static final String JSON_LINE_THICKNESS = "line_thickness";
    static final String JSON_LINE_BEGINNING = "line_beginning";
    static final String JSON_LINE_ENDING = "line_ending";
    //DraggableLabel
    static final String JSON_MAP_LABELS = "labels";
    static final String JSON_FONT_TEXT = "font_text";
    static final String JSON_FONT_FAMILY = "font_family";
    static final String JSON_FONT_SIZE = "font_size";
    static final String JSON_BOLD = "font_bold";
    static final String JSON_ITALICIZE = "font_italicize";
    static final String JSON_FONT_COLOR = "font_color";
    //DraggableImageView
    static final String JSON_MAP_IMAGES = "images";
    static final String JSON_IMAGE_PATH = "image_path";
    static final String DEFAULT_DOCTYPE_DECLARATION = "<!doctype html>\n";
    static final String DEFAULT_ATTRIBUTE_VALLUE = "";
    
    
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
	mmmData dataManager = (mmmData)data;
        //canvas properties
        
        JsonObject bgColorJson = makeJsonColorObject(dataManager.getBackgroundColor());
        String bgImagePath = dataManager.getBackgroundImagePath();
        boolean usingBGImage = dataManager.usingBGImage();
        //lines
        JsonArrayBuilder linesBuilder = Json.createArrayBuilder();
        ArrayList<DraggableLineEnd> lines = dataManager.getLines();
        for (DraggableLineEnd line: lines){
            String name = line.getName();
            JsonObject colorJson = makeJsonColorObject(line.getFillLine()); 
            JsonArrayBuilder stationsBuilder = Json.createArrayBuilder();
            for (DraggableStation station: line.getChildren()){
                String stationName = station.getName().getText();
                stationsBuilder.add(stationName);
            }
            double endX = line.getEnding().xProperty().doubleValue();
            double endY = line.getEnding().yProperty().doubleValue();
            JsonObject JsonLineBeginning = Json.createObjectBuilder()
                    .add(JSON_X, line.getBeginning().xProperty().doubleValue())
                    .add(JSON_Y, line.getBeginning().yProperty().doubleValue())
                    .add(JSON_FONT_FAMILY, line.getBeginning().getFontFamily())
                    .add(JSON_FONT_SIZE, line.getBeginning().getFontSize())
                    .add(JSON_BOLD, line.getBeginning().isBold())
                    .add(JSON_ITALICIZE, line.getEnding().isItalic())
                    .build();
            JsonObject JsonLineEnding = Json.createObjectBuilder()
                    .add(JSON_X, line.getEnding().xProperty().doubleValue())
                    .add(JSON_Y, line.getEnding().yProperty().doubleValue())
                    .add(JSON_FONT_FAMILY, line.getBeginning().getFontFamily())
                    .add(JSON_FONT_SIZE, line.getBeginning().getFontSize())
                    .add(JSON_BOLD, line.getBeginning().isBold())
                    .add(JSON_ITALICIZE, line.getEnding().isItalic())
                    .build();
            JsonObject JsonLine = Json.createObjectBuilder()
                    .add(JSON_NAME,name)
                    .add(JSON_CIRCULAR,line.isCircular())
                    .add(JSON_COLOR,colorJson)
                    .add(JSON_LINE_THICKNESS, line.getLineWidth())
                    .add(JSON_LINE_STATIONS,stationsBuilder)
                    .add(JSON_LINE_BEGINNING, JsonLineBeginning)
                    .add(JSON_LINE_ENDING, JsonLineEnding)
                    .build();
            linesBuilder.add(JsonLine);
        }
        //stations
        JsonArrayBuilder stationsBuilder = Json.createArrayBuilder();
        ArrayList<DraggableStation> stations = dataManager.getStations();
        for (DraggableStation station: stations){
            String name = station.getName().getText();
            double x = station.getCenterX();
            double y = station.getCenterY();
            JsonObject fillColor = makeJsonColorObject((Color) station.getFill());
            JsonObject fontColor = makeJsonColorObject((Color)station.getName().getFill());
            JsonObject JsonStation = Json.createObjectBuilder()
                    .add(JSON_NAME,name)
                    .add(JSON_X,x)
                    .add(JSON_Y,y)
                    .add(JSON_COLOR, fillColor)
                    .add(JSON_RADIUS, station.getRadiusX())
                    .add(JSON_STATION_ROTATION, station.getRot())
                    .add(JSON_STATION_POSITION, station.getPos())
                    .add(JSON_FONT_FAMILY, station.getFontFamily())
                    .add(JSON_FONT_SIZE, station.getFontSize())
                    .add(JSON_BOLD, station.isBold())
                    .add(JSON_ITALICIZE, station.isItalic())
                    .add(JSON_FONT_COLOR,fontColor)
                    .build();
            stationsBuilder.add(JsonStation);
        }
        JsonArrayBuilder labelsBuilder = Json.createArrayBuilder();
        JsonArrayBuilder imageViewBuilder = Json.createArrayBuilder();
        for (Node node: dataManager.getNodes()){
            //labels
            if (node instanceof DraggableLabel && !((DraggableLabel)node).isLineEnd()){
                JsonObject fillColor = makeJsonColorObject((Color) ((DraggableLabel)node).getFill());
                JsonObject JsonLabel = Json.createObjectBuilder()
                        .add(JSON_NAME, ((DraggableLabel)node).getText())
                        .add(JSON_X, ((DraggableLabel)node).getX())
                        .add(JSON_Y, ((DraggableLabel)node).getY())
                        .add(JSON_COLOR, fillColor)
                        .add(JSON_FONT_FAMILY, ((DraggableLabel)node).getFontFamily())
                        .add(JSON_FONT_SIZE, ((DraggableLabel)node).getFontSize())
                        .add(JSON_BOLD, ((DraggableLabel)node).isBold())
                        .add(JSON_ITALICIZE, ((DraggableLabel)node).isItalic())
                        .build();
                labelsBuilder.add(JsonLabel);
            }
            //imageview
            if (node instanceof DraggableImageView){
                JsonObject JsonImageView = Json.createObjectBuilder()
                        .add(JSON_IMAGE_PATH, ((DraggableImageView)node).getPath())
                        .add(JSON_X, ((DraggableImageView) node).getX())
                        .add(JSON_Y, ((DraggableImageView) node).getY())
                        .build();
                imageViewBuilder.add(JsonImageView);
            }
        }
        //combine all components
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_NAME, dataManager.getFileName())
                .add(JSON_BG_COLOR, bgColorJson)
                .add(JSON_BG_IMAGE_PATH, bgImagePath)
                .add(JSON_USING_IMAGE, usingBGImage)
                .add(JSON_LINES, linesBuilder)
                .add(JSON_MAP_STATIONS, stationsBuilder)
                .add(JSON_MAP_LABELS, labelsBuilder)
                .add(JSON_MAP_IMAGES, imageViewBuilder)
                .build();
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();
        
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
    }
    
     @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        mmmData dataManager = (mmmData)data;
        dataManager.resetData();
        System.out.println("starting load data");
        JsonObject json = loadJSONFile(filePath);
        System.out.println("load json");
        //fileName
        String name = json.getString(JSON_NAME);
        dataManager.setFileName(name);
        ((mmmWorkspace)dataManager.getApp().getWorkspaceComponent()).setFileName(name);
        System.out.println("loaded name");
        //background
        boolean usingBGImage = json.getBoolean(JSON_USING_IMAGE);
        System.out.println("loaded using image");
        Color bgColor = loadColor(json, JSON_BG_COLOR);
        System.out.println("loaded color");
        Image bgImage = new Image(json.getString(JSON_BG_IMAGE_PATH));
        System.out.println("loaded image");
        if (usingBGImage) {
            dataManager.setBackgroundColor(bgColor);
            dataManager.setBackgroundImage(bgImage, json.getString(JSON_BG_IMAGE_PATH));
        } else {
            dataManager.setBackgroundImage(bgImage, json.getString(JSON_BG_IMAGE_PATH));
            dataManager.setBackgroundColor(bgColor);
        }
        System.out.println("loaded background");
        //stations
        JsonArray jsonMapStationsArray = json.getJsonArray(JSON_MAP_STATIONS);
        for (int i = 0; i < jsonMapStationsArray.size(); i++) {
            JsonObject jsonStation = jsonMapStationsArray.getJsonObject(i);
            DraggableStation station = loadStation(jsonStation);
            dataManager.getStations().add(station);
            ((mmmWorkspace)(dataManager.getApp().getWorkspaceComponent())).getStations().add(station.getName().getText());
        }
        System.out.println("loaded stations");
        //lines
        JsonArray jsonLineArray = json.getJsonArray(JSON_LINES);
        for (int i = 0; i < jsonLineArray.size(); i++) {
            JsonObject jsonLine = jsonLineArray.getJsonObject(i);
            DraggableLineEnd line = loadLine(jsonLine);
            System.out.println("loaded line succeeded");
            dataManager.getLines().add(line);
            dataManager.addNode(line.getBeginning());
            dataManager.addNode(line.getEnding());
            ((mmmWorkspace)(dataManager.getApp().getWorkspaceComponent())).getLines().add(line.getName());
            JsonArray jsonStationArray = jsonLine.getJsonArray(JSON_LINE_STATIONS);
            for (int j = 0; j < jsonStationArray.size(); j++) {
                String s = jsonStationArray.getString(j);
                for (int k = 0; k < dataManager.getStations().size(); k++){
                    if (s.equals(((DraggableStation)(dataManager.getStations().get(k))).getName().getText())){
                        line.getChildren().add((DraggableStation)(dataManager.getStations().get(k)));
                    }
                }
            }
            if (jsonStationArray.isEmpty()){
                Line connecting_station = line.formLine(line.getBeginning(),line.getEnding(),line.getFillLine());
                dataManager.addNode(connecting_station);
                line.getConnectors().add(connecting_station);
            } else {
                Line begin_to_station = line.formLine(line.getBeginning(), line.getChildren().get(0), line.getFillLine());
                dataManager.addNode(begin_to_station);
                line.getConnectors().add(begin_to_station);
                System.out.println("Works up to here");
                System.out.println(jsonStationArray.size());
                for (int k = 1; k < jsonStationArray.size(); k++){
                    Line l = line.formLine(line.getChildren().get(k-1), line.getChildren().get(k), line.getFillLine());
                    dataManager.addNode(l);
                    line.getConnectors().add(l);
                    System.out.println(k);
                }
                Line station_to_end = line.formLine(line.getChildren().get(line.getChildren().size()-1), line.getEnding(), line.getFillLine());
                dataManager.addNode(station_to_end);
                line.getConnectors().add(station_to_end);
                System.out.println(line.getChildren().size());
            }
                    
        }
        System.out.println("loaded lines");
        //labels
        JsonArray jsonLabelsArray = json.getJsonArray(JSON_MAP_LABELS);
        for (int i = 0; i < jsonLabelsArray.size(); i++) {
            JsonObject jsonLabel = jsonLabelsArray.getJsonObject(i);
            DraggableLabel label = loadLabel(jsonLabel);
            dataManager.addNode(label);
        }
        //imageview
        JsonArray jsonImageArray = json.getJsonArray(JSON_MAP_IMAGES);
        for (int i = 0; i < jsonImageArray.size(); i++) {
            JsonObject jsonImage = jsonImageArray.getJsonObject(i);
            DraggableImageView imageView = loadImageView(jsonImage);
            dataManager.addNode(imageView);
        }
        for (int i = 0; i < dataManager.getStations().size(); i++){
            dataManager.getNodes().add((DraggableStation)dataManager.getStations().get(i));
            dataManager.getNodes().add(((DraggableStation)dataManager.getStations().get(i)).getName());
        }
    }
    
    private DraggableLineEnd loadLine(JsonObject jsonLine){
        String name = jsonLine.getString(JSON_NAME);
        Color color = loadColor(jsonLine, JSON_COLOR);
        double lineWidth = getDataAsDouble(jsonLine,JSON_LINE_THICKNESS);
        DraggableLineEnd line = new DraggableLineEnd(color,name,lineWidth);
        boolean circular = jsonLine.getBoolean(JSON_CIRCULAR);
        System.out.println("inside loadline: line load succeeded");
        if (circular) {
            line.toggleCircular();
        };
        JsonObject beginning = jsonLine.getJsonObject(JSON_LINE_BEGINNING);
        double begin_x = getDataAsDouble(beginning, JSON_X);
        double begin_y = getDataAsDouble(beginning, JSON_Y);
        String begin_font_family = beginning.getString(JSON_FONT_FAMILY);
        double begin_font_size = getDataAsDouble(beginning, JSON_FONT_SIZE);
        boolean begin_bold = beginning.getBoolean(JSON_BOLD);
        boolean begin_italicize = beginning.getBoolean(JSON_ITALICIZE);
        line.getBeginning().start((int)begin_x, (int)begin_y);
        line.getBeginning().setX(begin_x);
        line.getBeginning().setY(begin_y);
        line.getBeginning().changeFont(begin_font_family,begin_font_size,begin_bold,begin_italicize);
        JsonObject ending = jsonLine.getJsonObject(JSON_LINE_ENDING);
        double end_x = getDataAsDouble(ending, JSON_X);
        double end_y = getDataAsDouble(ending, JSON_Y);
        String end_font_family = ending.getString(JSON_FONT_FAMILY);
        double end_font_size = getDataAsDouble(ending, JSON_FONT_SIZE);
        boolean end_bold = ending.getBoolean(JSON_BOLD);
        boolean end_italicize = ending.getBoolean(JSON_ITALICIZE);
        line.getEnding().start((int)end_x, (int)end_y);
        line.getEnding().setX(end_x);
        line.getEnding().setY(end_y);
        line.getEnding().changeFont(end_font_family,end_font_size,end_bold,end_italicize);
        line.changeLineThick(lineWidth);
        System.out.println("inside loadline: line loaded");
        return line;
    }
    
    private DraggableStation loadStation(JsonObject jsonStation) {
        String name = jsonStation.getString(JSON_NAME);
        DraggableStation station = new DraggableStation(name);
        double x = getDataAsDouble(jsonStation, JSON_X);
        double y = getDataAsDouble(jsonStation, JSON_Y);
        station.start((int)x, (int)y);
        station.setCenterX(x);
        station.setCenterY(y);
        Color color = loadColor(jsonStation, JSON_COLOR);
        station.setFill(color);
        double radius = getDataAsDouble(jsonStation, JSON_RADIUS);
        station.setRadiusX(radius);
        station.setRadiusY(radius);
        int rot = jsonStation.getInt(JSON_STATION_ROTATION);
        station.setRot(rot);
        int pos = jsonStation.getInt(JSON_STATION_POSITION);
        station.setPos(pos);
        String fontFamily = jsonStation.getString(JSON_FONT_FAMILY);
        double fontSize = getDataAsDouble(jsonStation, JSON_FONT_SIZE);
        boolean bold = jsonStation.getBoolean(JSON_BOLD);
        boolean italicize = jsonStation.getBoolean(JSON_ITALICIZE);
        station.changeFont(fontFamily, fontSize, bold, italicize);
        Color fontColor = loadColor(jsonStation, JSON_FONT_COLOR);
        station.getName().setFill(fontColor);
        return station;
    }
    
    private DraggableLabel loadLabel(JsonObject jsonLabel) {
        String name = jsonLabel.getString(JSON_NAME);
        DraggableLabel label = new DraggableLabel(name);
        double x = getDataAsDouble(jsonLabel, JSON_X);
        double y = getDataAsDouble(jsonLabel, JSON_Y);
        label.start((int)x, (int)y);
        label.setX(x);
        label.setY(y);
        String fontFamily = jsonLabel.getString(JSON_FONT_FAMILY);
        double fontSize = getDataAsDouble(jsonLabel, JSON_FONT_SIZE);
        boolean bold = jsonLabel.getBoolean(JSON_BOLD);
        boolean italicize = jsonLabel.getBoolean(JSON_ITALICIZE);
        label.changeFont(fontFamily, fontSize, bold, italicize);
        Color color = loadColor(jsonLabel, JSON_COLOR);
        label.setFill(color);
        System.out.println("Label Loaded");
        return label;
    }
    
    private DraggableImageView loadImageView(JsonObject jsonImage) {
        String path = jsonImage.getString(JSON_IMAGE_PATH);
        Image i = new Image(path);
        DraggableImageView imageView = new DraggableImageView(i);
        double x = getDataAsDouble(jsonImage, JSON_X);
        double y = getDataAsDouble(jsonImage, JSON_Y);
        imageView.start((int)x, (int)y);
        imageView.setX(x);
        imageView.setY(y);
        return imageView;
    }
    
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        
        mmmData dataManager = (mmmData)data;
        JsonArrayBuilder linesBuilder = Json.createArrayBuilder();
        ArrayList<DraggableLineEnd> lines = dataManager.getLines();
        for (DraggableLineEnd line: lines){
            String name = line.getName();
            JsonObject colorJson = makeJsonColorObject(line.getFillLine()); 
            JsonArrayBuilder stationsBuilder = Json.createArrayBuilder();
            for (DraggableStation station: line.getChildren()){
                String stationName = station.getName().getText();
                stationsBuilder.add(stationName);
            }
            JsonObject JsonLine = Json.createObjectBuilder()
                    .add(JSON_NAME,name)
                    .add(JSON_CIRCULAR,line.isCircular())
                    .add(JSON_COLOR,colorJson)
                    .add(JSON_LINE_STATIONS,stationsBuilder)
                    .build();
            linesBuilder.add(JsonLine);
        }
        JsonArrayBuilder stationsBuilder = Json.createArrayBuilder();
        ArrayList<DraggableStation> stations = dataManager.getStations();
        for (DraggableStation station: stations){
            String name = station.getName().getText();
            double x = station.getCenterX();
            double y = station.getCenterY();
            JsonObject JsonStation = Json.createObjectBuilder()
                    .add(JSON_NAME,name)
                    .add(JSON_X,x)
                    .add(JSON_Y,y)
                    .build();
            stationsBuilder.add(JsonStation);
        }
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_NAME, dataManager.getFileName())
                .add(JSON_LINES, linesBuilder)
                .add(JSON_MAP_STATIONS, stationsBuilder)
                .build();
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();
        
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    private JsonObject makeJsonColorObject(Color color) {
	JsonObject colorJson = Json.createObjectBuilder()
		.add(JSON_RED, color.getRed())
		.add(JSON_GREEN, color.getGreen())
		.add(JSON_BLUE, color.getBlue())
		.add(JSON_ALPHA, color.getOpacity()).build();
	return colorJson;
    }

     private double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
    
    private String getDataAsString(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonString string = (JsonString)value;
	return string.getString();
    }
    private Color loadColor(JsonObject json, String colorToGet) {
	JsonObject jsonColor = json.getJsonObject(colorToGet);
	double red = getDataAsDouble(jsonColor, JSON_RED);
	double green = getDataAsDouble(jsonColor, JSON_GREEN);
	double blue = getDataAsDouble(jsonColor, JSON_BLUE);
	double alpha = getDataAsDouble(jsonColor, JSON_ALPHA);
	Color loadedColor = new Color(red, green, blue, alpha);
	return loadedColor;
    }
    
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
}
