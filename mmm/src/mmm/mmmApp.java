package mmm;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Locale;
import mmm.data.mmmData;
import mmm.file.mmmFiles;
import mmm.gui.mmmWorkspace;
import djf.AppTemplate;
import static djf.settings.AppPropertyType.APP_TITLE;
import static djf.settings.AppPropertyType.PROPERTIES_LOAD_ERROR_MESSAGE;
import static djf.settings.AppPropertyType.PROPERTIES_LOAD_ERROR_TITLE;
import static djf.settings.AppStartupConstants.APP_PROPERTIES_FILE_NAME;
import djf.ui.AppGUI;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppYesNoCancelDialogSingleton;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.application.Application.*;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import static mmm.css.mmmStyle.*;
import properties_manager.PropertiesManager;
/**
 *
 * @author shawn
 */
public class mmmApp extends AppTemplate {
     /**
     * This hook method must initialize all three components in the
     * proper order ensuring proper dependencies are respected, meaning
     * all proper objects are already constructed when they are needed
     * for use, since some may need others for initialization.
     */
    @Override
    public void buildAppComponentsHook() {
        // CONSTRUCT ALL THREE COMPONENTS. NOTE THAT FOR THIS APP
        // THE WORKSPACE NEEDS THE DATA COMPONENT TO EXIST ALREADY
        // WHEN IT IS CONSTRUCTED, AND THE DATA COMPONENT NEEDS THE
        // FILE COMPONENT SO WE MUST BE CAREFUL OF THE ORDER
        System.out.println("buildAppComponentsHook has been called");
        fileComponent = new mmmFiles();
        System.out.println("fileComponent has been instantiated");
        dataComponent = new mmmData(this);
        System.out.println("dataComponent has been instantiated");
        workspaceComponent = new mmmWorkspace(this);
        System.out.println("workspaceComponent has been instantiated");
    }
    @Override
    public void start(Stage primaryStage) {
	// LET'S START BY INITIALIZING OUR DIALOGS
	AppMessageDialogSingleton messageDialog = AppMessageDialogSingleton.getSingleton();
	messageDialog.init(primaryStage);
	AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
	yesNoDialog.init(primaryStage);
	PropertiesManager props = PropertiesManager.getPropertiesManager();

	try {
            //-fx-background-color: #c8e0cb;-fx-background-color: #d2eefb;
	    // LOAD APP PROPERTIES, BOTH THE BASIC UI STUFF FOR THE FRAMEWORK
	    // AND THE CUSTOM UI STUFF FOR THE WORKSPACE
	    boolean success = loadProperties(APP_PROPERTIES_FILE_NAME);
	    if (success) {
                // GET THE TITLE FROM THE XML FILE
		String appTitle = props.getProperty(APP_TITLE);
                // BUILD THE BASIC APP GUI OBJECT FIRST
		gui = new AppGUI(primaryStage, appTitle, this);
                primaryStage.getIcons().add(new Image("file:./images/MMMLogo.png"));
                buildAppComponentsHook();
                initWelcome(primaryStage);
                // THIS BUILDS ALL OF THE COMPONENTS, NOTE THAT
                // IT WOULD BE DEFINED IN AN APPLICATION-SPECIFIC
                // CHILD CLASS
                
                    // NOW OPEN UP THE WINDOW
            } 
	}catch (Exception e) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(PROPERTIES_LOAD_ERROR_TITLE), props.getProperty(PROPERTIES_LOAD_ERROR_MESSAGE));
	}
    }
    
    private void initWelcome(Stage primaryStage){
        Stage dialog = new Stage();
        dialog.initStyle(StageStyle.UTILITY);
        dialog.setTitle("Metro Map Maker: Start Window");
        Pane welcomePane = new Pane();
        Scene scene = new Scene(welcomePane);
        HBox layout = new HBox();
        layout.setStyle("-fx-background-color: linear-gradient(to right, #c8e0cb, #d2eefb);");
        VBox firstCol = new VBox();
        firstCol.setPrefWidth(350);
        firstCol.setPrefHeight(700);
        firstCol.setStyle("-fx-padding: 80px 20px 20px 20px;");
        HBox header = new HBox();
        Text headerText = new Text("Recent Work");
        VBox recentBox = new VBox();
        File folder = new File("./work");
        File[] listOfFiles = folder.listFiles();
        for (File listOfFile : listOfFiles) {
            if (listOfFile.isFile()) {
                Button b = new Button(listOfFile.getName());
                b.setStyle("-fx-pref-width: 350px;"+
                        "-fx-pref-height: 50px;"+
                        "-fx-background-color: #0039a6;" +
                        "-fx-padding: 10px 15px 10px 15px;" +
                        "-fx-border-insets: 5px;" +
                        "-fx-background-insets: 5px;" +
                        "-fx-border-width: 5px;" +
                        "-fx-border-color: #FFFFFF;" +
                        "-fx-font-weight: bold;" +
                        "-fx-text-fill: #FFFFFF;");
                b.setOnAction(e->{ 
                    File f = new File("./work/" + b.getText());
                    try {
                        this.getWorkspaceComponent().activateWorkspace(this.getGUI().getAppPane());
                        this.getFileComponent().loadData(dataComponent, f.getAbsolutePath());
                        this.getGUI().updateToolbarControls(true);
                        dialog.close();
                    } catch (IOException ex) {
                        Logger.getLogger(mmmApp.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                recentBox.getChildren().add(0,b);
            }
        }
        headerText.setFill(Color.WHITE);
        headerText.setStroke(Paint.valueOf("#0039a6"));
        headerText.setStrokeWidth(1.20);
        headerText.setFont(Font.font("Arial,Helvetica,sans-serif;",FontWeight.BOLD, FontPosture.REGULAR, 30));
        header.getChildren().add(headerText);
        firstCol.getChildren().add(header);
        HBox transition = new HBox();
        transition.setStyle("-fx-width: 310px;-fx-height: 20px;-fx-border-width: 2px 0px 0px 0px;-fx-border-color: #0039a6;");
        firstCol.getChildren().add(transition);
        firstCol.getChildren().add(recentBox);
        VBox secondCol = new VBox();
        secondCol.setPrefWidth(550);
        secondCol.setPrefHeight(700);
        secondCol.setStyle("-fx-padding: 80px 20px 20px 20px;");
        HBox col2row1 = new HBox();
        col2row1.setStyle("-fx-padding: 0px 80px 80px 80px;");
        Image logo = new Image("file:./images/MMMLogo.png");
        ImageView logoView = new ImageView(logo);
        logoView.setFitWidth(350);
        logoView.setFitHeight(350);
        col2row1.getChildren().add(logoView);
        HBox col2row2 = new HBox();
        Button createNew = new Button("CREATE A NEW MAP");
        createNew.setStyle("-fx-pref-width: 350px;"+
                            "-fx-pref-height: 50px;"+
                            "-fx-background-color: #0039a6;" +
                            "-fx-padding: 10px 15px 10px 15px;" +
                            "-fx-border-insets: 5px;" +
                            "-fx-background-insets: 5px;" +
                            "-fx-border-width: 5px;" +
                            "-fx-border-color: #FFFFFF;" +
                            "-fx-font-weight: bold;" +
                            "-fx-text-fill: #FFFFFF;");
                
                
        col2row2.setStyle("-fx-padding: 0px 80px 80px 80px;");
        col2row2.getChildren().add(createNew);
        secondCol.getChildren().add(col2row1);
        secondCol.getChildren().add(col2row2);
        layout.getChildren().add(firstCol);
        layout.getChildren().add(secondCol);
        welcomePane.getChildren().add(layout);
        dialog.setScene(scene);
        createNew.setOnAction(e -> {
            TextInputDialog fileDialog = new TextInputDialog("NewMap");
            fileDialog.setTitle("New Map Name");
            fileDialog.setHeaderText("");
            fileDialog.setContentText("Enter Map Name:");
            Optional<String> result = fileDialog.showAndWait();
            result.ifPresent(name -> ((mmmWorkspace)this.getWorkspaceComponent()).setFileName(result.toString().substring(9, result.toString().length()-1)));
            Path p = Paths.get("./export/" + ((mmmWorkspace)this.getWorkspaceComponent()).getFileName());
            int i = 0;
            while (Files.exists(p)){
                i += 1;
                p = Paths.get("./export/" + ((mmmWorkspace)this.getWorkspaceComponent()).getFileName() + "(" + i + ")");
            }
            if (i == 0){
                ((mmmWorkspace)this.getWorkspaceComponent()).setFileName(((mmmWorkspace)this.getWorkspaceComponent()).getFileName());
            }
            else {
                ((mmmWorkspace)this.getWorkspaceComponent()).setFileName(((mmmWorkspace)this.getWorkspaceComponent()).getFileName()  + "(" + i + ")");
            }
            new File("./export/" + ((mmmWorkspace)this.getWorkspaceComponent()).getFileName()).mkdirs();
            p = Paths.get("./export/" + ((mmmWorkspace)this.getWorkspaceComponent()).getFileName() + "/" + ((mmmWorkspace)this.getWorkspaceComponent()).getFileName()  + " Metro");
            try {
                Files.createFile(p);
            } catch (IOException ex) {
                System.err.format("createFile error: %s%n", ex);
            }
            ((mmmData)this.getDataComponent()).setFileName(((mmmWorkspace)this.getWorkspaceComponent()).getFileName());
            this.gui.getFileController().handleNewRequest();
            dialog.close();
        });
        dialog.showAndWait();
        primaryStage.show();        
    }
    
    /**
     * This is where program execution begins. Since this is a JavaFX app it
     * will simply call launch, which gets JavaFX rolling, resulting in sending
     * the properly initialized Stage (i.e. window) to the start method inherited
     * from AppTemplate, defined in the Desktop Java Framework.
     * @param args
     */
    public static void main(String[] args) {
	Locale.setDefault(Locale.US);
	launch(args);
    }
}
