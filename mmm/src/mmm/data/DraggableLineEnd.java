/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;
import java.util.ArrayList;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeLineJoin;

/**
 *
 * @author shawn
 */
public class DraggableLineEnd extends Line implements Draggable{
    DraggableLabel begin;
    DraggableLabel end;
    Color color;
    String name;
    Boolean circular;
    ArrayList<DraggableStation> station_names;
    ArrayList<Line> connecting_stations;
    double lineWidth;
    public DraggableLineEnd(){
        
    }
    
    public DraggableLineEnd(Color LineColor, String Name, double LineWidth){
        name = Name;
        color = LineColor;
        begin = new DraggableLabel(name + " - END");
        begin.setFill(color);
        begin.toggleLineEnd();
        begin.setDraggableLineEnd(this);
        end = new DraggableLabel(name + " - END");
        end.setFill(color);
        end.toggleLineEnd();
        end.setDraggableLineEnd(this);
        circular = false;
        station_names = new ArrayList<>();
        connecting_stations = new ArrayList<>();
        lineWidth = LineWidth;
    }
    
    public ArrayList<DraggableStation> getChildren(){
        return station_names;
    }
    
    public ArrayList<Line> getConnectors(){
        return connecting_stations;
    }
    
    public void toggleCircular(){
        circular = !circular;
    }
    
    @Override
    public mmmState getStartingState() {
        return mmmState.STARTING_LINE;
    }

    @Override
    public void start(int x, int y) {
        begin.start(x - 100, y);
        begin.setX(x-100);
        begin.setY(y);
        end.start(x + 100, y);
        end.setX(x + 100);
        end.setY(y);
        Line start = formLine(begin,end,color);
        connecting_stations.add(start);
    }

    @Override
    public void drag(int x, int y) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void size(int x, int y) {
        
    }

    @Override
    public double getWidth() {
        return 0;
    }

    @Override
    public double getHeight() {
        return 0;
    }

    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
        begin.setLocationAndSize(initX-100,initY,initWidth,initHeight);
        end.setLocationAndSize(initX+100, initY, initWidth, initHeight);
    }

    @Override
    public String getShapeType() {
        return LINEEND;
    }

    @Override
    public double getX() {
        return 0;
    }

    @Override
    public double getY() {
        return 0;
    }
    public DraggableLabel getBeginning(){
        return begin;
    }
    public DraggableLabel getEnding(){
        return end;
    }
    public Line formLine(Draggable firstStation, Draggable nextStation, Color Color){
        Line line = new Line(firstStation.getX(),firstStation.getY(),nextStation.getX(),nextStation.getY());
        if (firstStation instanceof DraggableStation){
            line.startXProperty().bind(((DraggableStation)firstStation).centerXProperty());
            line.startYProperty().bind(((DraggableStation)firstStation).centerYProperty());
        } else if (firstStation instanceof DraggableLabel) {
            line.startXProperty().bind(((DraggableLabel)firstStation).xProperty().add(((DraggableLabel)firstStation).getBoundsInLocal().getWidth()/2));
            line.startYProperty().bind(((DraggableLabel)firstStation).yProperty().add(((DraggableLabel)firstStation).getBoundsInLocal().getHeight()/2));
        }
        if (nextStation instanceof DraggableStation){
            line.endXProperty().bind(((DraggableStation)nextStation).centerXProperty());
            line.endYProperty().bind(((DraggableStation)nextStation).centerYProperty());
        } else if (nextStation instanceof DraggableLabel) {
            line.endXProperty().bind(((DraggableLabel)nextStation).xProperty().add(((DraggableLabel)nextStation).getBoundsInLocal().getWidth()/2));
            line.endYProperty().bind(((DraggableLabel)nextStation).yProperty().subtract(((DraggableLabel)nextStation).getBoundsInLocal().getHeight()));
        }
        line.getStrokeDashArray().addAll(20d);
        line.setFill(Color.BLACK);
        line.setStroke(Color);
        line.setStrokeWidth(lineWidth);
        line.setStrokeLineJoin(StrokeLineJoin.ROUND);
        line.setStrokeLineCap(StrokeLineCap.ROUND);
        return line;
    }
    
    public void changeFillLine(Color colorLine){
        for(Line l:connecting_stations){
            l.setStroke(colorLine);
            l.setFill(colorLine);
        }
        begin.setFill(colorLine);
        end.setFill(colorLine);
        color = colorLine;
    }
    
    public void changeLineThick(double d){
        for (Line l:connecting_stations){
            l.setStrokeWidth(d);
        }
        lineWidth = d;
    }
    
    @Override
    public String toString(){
        return name;
    }
    
    public Color getFillLine(){
        return color;
    }
    
    public String getName() {
        return name;
    }
    public void setName(String s) {
        name = s;
    }
    public boolean isCircular(){
        return circular;
    }
    public double getLineWidth(){
        return lineWidth;
    }
}
