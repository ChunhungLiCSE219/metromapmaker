/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

/**
 *
 * @author shawn
 */

import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeLineJoin;
import javafx.scene.shape.StrokeType;
import mmm.gui.CanvasController;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.Text;

/**
 * This interface represents a family of draggable shapes.
 * 
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class DraggableLabel extends Text implements Draggable{
    double startCenterX;
    double startCenterY;
    private boolean bold;
    private boolean italic;
    private String fontFamily;
    private Double fontSize;
    public boolean lineEnd;
    public DraggableLineEnd line;
    public DraggableLabel(){
        
    }
    public DraggableLabel(String text){
        setText(text);
        bold = true;
        italic = false;
        fontFamily = "Verdana";
        fontSize = 16.0;
        lineEnd = false;
        setStrokeLineJoin(StrokeLineJoin.ROUND);
        setStrokeLineCap(StrokeLineCap.ROUND);
        setStrokeType(StrokeType.OUTSIDE);
        setStroke(Color.BLACK);
        setStrokeWidth(1.2);
        setFont(Font.font(fontFamily, fontSize));
    }
    
    @Override
    public mmmState getStartingState(){
        return mmmState.STARTING_TEXT;
    }
    @Override
    public void start(int x, int y){
        startCenterX = x;
        startCenterY = y;
    }
    @Override
    public void drag(int x, int y){
        double diffX = x - startCenterX;
        double diffY = y - startCenterY;
        double newX = startCenterX + diffX;
	double newY = startCenterY + diffY;
        if (newX > 0 && newX + getWidth()< 1560 && newY> 0 && newY + getHeight() < 900){
            xProperty().set(newX - CanvasController.getInitialX());
            yProperty().set(newY - CanvasController.getInitialY());
            startCenterX = newX;
            startCenterY = newY;
        }
    }
    
    public void size(int x, int y){
        
    }
    
    public void changeFont(String fontFamily, double size, boolean bold, boolean italics){
        setFontFamily(fontFamily);
        setFontSize(size);
        this.bold = bold;
        this.italic = italics;
        if (bold && italics){
            setFont(Font.font(fontFamily, FontWeight.BOLD, FontPosture.ITALIC, size));
        } else if (bold && !italics){
            setFont(Font.font(fontFamily, FontWeight.BOLD, size));
        } else if (!bold && italics){
            setFont(Font.font(fontFamily, FontPosture.ITALIC, size));
        } else {
           setFont(Font.font(fontFamily, size));
        }
    }
    
    @Override
    public double getWidth(){
        return getWrappingWidth();
    }
    @Override
    public double getHeight(){
        return 0;
    }
    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight){
        xProperty().set(initX);
	yProperty().set(initY);
    }
    @Override
    public String getShapeType(){
        return LABEL;
    }
    public boolean isBold(){
        return bold;
    }
    public boolean isItalic(){
        return italic;
    }
    public void toggleBold(){
        bold = !bold;
    }
    public void toggleItalic(){
        italic = !italic;
    }
    public void setFontFamily(String ff){
        fontFamily = ff;
    }
    public void setFontSize(double fs){
        fontSize = fs;
    }
    public String getFontFamily(){
        return fontFamily;
    }
    public Double getFontSize(){
        return fontSize;
    }
    public DraggableLabel deepCopy(double x, double y){
        DraggableLabel copy = new DraggableLabel(getText());
        copy.setFont(getFont());
        copy.start((int)x,(int)y);
        copy.setFill(getFill());
        copy.setStroke(getStroke());
        copy.setStrokeWidth(getStrokeWidth());
        return copy;
    }
    public void toggleLineEnd(){
        lineEnd = true;
    }
    public void setDraggableLineEnd(DraggableLineEnd Line){
        line = Line;
    }
    public DraggableLineEnd getLine(){
        return line;
    }
    @Override
    public String toString(){
        return getText();
    }
    public boolean isLineEnd(){
        return lineEnd;
    }
    public void setBold(boolean b){
        bold = b;
    }
    public void setItalics(boolean i){
        italic = i;
    }
}

