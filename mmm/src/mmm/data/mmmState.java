/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

/**
 *
 * @author shawn
 */
public enum mmmState {
    SELECTING_SHAPE, 
    STARTING_IMAGE, 
    STARTING_TEXT, 
    STARTING_LINE, 
    STARTING_STATION,
    DRAGGING_SHAPE,
    DRAGGING_NOTHING,
    ADD_STATION,
    REMOVE_STATION,
}
