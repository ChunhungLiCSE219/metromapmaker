/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Shape;
import static mmm.data.mmmState.*;
import mmm.gui.mmmWorkspace;
import djf.components.AppDataComponent;
import djf.AppTemplate;
import javafx.scene.image.Image;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

/**
 *
 * @author shawn
 */
public class mmmData implements AppDataComponent {
    AppTemplate app;
    Node selectedNode;
    Node newNode;
    ObservableList<Node> nodes;
    ArrayList<DraggableStation> stations;
    ArrayList<DraggableLineEnd> lines;
    ArrayList<Line> gridLines;
    Color currentFillColor;
    Color currentOutlineColor;
    double currentBorderWidth;
    Effect highlightedEffect;
    mmmState state;
    public static final String WHITE_HEX = "#FFFFFF";
    public static final String BLACK_HEX = "#000000";
    public static final String YELLOW_HEX = "#EEEE00";
    public static final Paint DEFAULT_BACKGROUND_COLOR = Paint.valueOf(WHITE_HEX);
    public static final Paint HIGHLIGHTED_COLOR = Paint.valueOf(YELLOW_HEX);
    public static final int HIGHLIGHTED_STROKE_THICKNESS = 3;
    public Color backgroundColor;
    public Image backgroundImage;
    String backgroundImagePath;
    boolean usingBGImage;
    boolean toggleGrid;
    String fileName;
    
    public mmmData(AppTemplate initApp) {
	// KEEP THE APP FOR LATER
	app = initApp;

        stations = new ArrayList();
        lines = new ArrayList();
        gridLines = new ArrayList();
        
	// NO SHAPE STARTS OUT AS SELECTED
	selectedNode = null;

	// INIT THE COLORS
	currentFillColor = Color.web(WHITE_HEX);
	currentOutlineColor = Color.web(BLACK_HEX);
	currentBorderWidth = 1;
	
	// THIS IS FOR THE SELECTED SHAPE
	DropShadow dropShadowEffect = new DropShadow();
	dropShadowEffect.setOffsetX(0.0f);
	dropShadowEffect.setOffsetY(0.0f);
	dropShadowEffect.setSpread(1.0);
	dropShadowEffect.setColor(Color.YELLOW);
	dropShadowEffect.setBlurType(BlurType.GAUSSIAN);
	dropShadowEffect.setRadius(15);
	highlightedEffect = dropShadowEffect;
        usingBGImage = false;
        toggleGrid = false;
        backgroundColor = Color.WHITE;
        backgroundImage = new Image("file:./images/MMMLogo.png");
        backgroundImagePath = "file:./images/MMMLogo.png";
    }
    
    public boolean usingBGImage(){
        return usingBGImage;
    }
    
    public ObservableList<Node> getNodes() {
	return nodes;
    }

    public Color getBackgroundColor() {
	return backgroundColor;
    }
    
    public String getBackgroundImagePath() {
        return backgroundImagePath;
    }
    
    public Color getCurrentFillColor() {
	return currentFillColor;
    }

    public Color getCurrentOutlineColor() {
	return currentOutlineColor;
    }

    public double getCurrentBorderWidth() {
	return currentBorderWidth;
    }
    
    public void addGrid(){
        if (gridLines.isEmpty()){
            Pane canvas = ((mmmWorkspace)app.getWorkspaceComponent()).getCanvas();
            double width = canvas.getWidth();
            double height = canvas.getHeight();
            for (double i = 0; i < width; i += 10){
                Line l = new Line(i,0,i,height);
                l.setFill(Color.BLACK);
                l.setStroke(Color.BLACK);
                l.setStrokeWidth(0.0);
                nodes.add(0,l);
                gridLines.add(l);
            }
            for (double i = 0; i < height; i += 10){
                Line l = new Line(0,i,width,i);
                l.setFill(Color.BLACK);
                l.setStroke(Color.BLACK);
                l.setStrokeWidth(0.0);
                nodes.add(0,l);
                gridLines.add(l);
            }
        }
    }
    
    public void toggleGrid(){
        if (gridLines.isEmpty()){
            addGrid();
        }
        for (int i = 0; i < gridLines.size(); i++){
            if (toggleGrid == false){
                gridLines.get(i).setStrokeWidth(.5);
            } else {
                gridLines.get(i).setStrokeWidth(0);
            }
        }
        toggleGrid = !toggleGrid;
    }
    public void setGridColor(Color color){
        for (int i = 0; i < gridLines.size(); i++){
            gridLines.get(i).setStroke(color);
        }
    }
    
    public void addNode(Node nodeToAdd){
        nodes.add(nodeToAdd);
    }
    
    public Node getTopNode(double x, double y){
        for (int i = nodes.size() - 1; i >= 0; i--) {
	    Node shape = (Node)nodes.get(i);
	    if (shape.contains(x, y)) {
		return shape;
	    }
	}
	return null;
    }
    public Node getSelectedNode() {
	return selectedNode;
    }
 

    public void setSelectedNode(Node initSelectedShape) {
	selectedNode = initSelectedShape;
    }
    
    public void highlightNode(Node node){
        node.setEffect(highlightedEffect);
    }
    public boolean isInState(mmmState testState){
        return state == testState;
    }
    public void removeSelectedNode(){
        if (selectedNode != null) {
	    nodes.remove(selectedNode);
            selectedNode = null;
	}
    }
    public void removeNode(Node nodeToRemove){
        nodes.remove(nodeToRemove);
    }
    @Override
    public void resetData() {
	setState(SELECTING_SHAPE);
	newNode = null;
	selectedNode = null;

	// INIT THE COLORS
	currentFillColor = Color.web(WHITE_HEX);
	currentOutlineColor = Color.web(BLACK_HEX);
	
	nodes.clear();
	((mmmWorkspace)app.getWorkspaceComponent()).getCanvas().getChildren().clear();
    }
    public Node selectTopNode(double x, double y){
        Node node = getTopNode(x, y);
        if (node instanceof Text && !(node instanceof DraggableLabel)){
            unhighlightNode(selectedNode);
        } else if (!(node instanceof Line)){
            if (node == selectedNode)
                return node;
            if (selectedNode != null) {
                unhighlightNode(selectedNode);
            }
            if (node != null) {
                highlightNode(node);
            }
            selectedNode = node;
            if (node != null) {
                ((Draggable)node).start((int)x, (int)y);
            }
            return node;
        } else {
            unhighlightNode(selectedNode);
        }
        return null;
    }

    public void setNodes(ObservableList<Node> initShapes) {
	nodes = initShapes;
    }
    public void unhighlightNode(Node node){
        selectedNode.setEffect(null);
        
    }
    public void setBackgroundColor(Color initBackgroundColor) {
        usingBGImage = false;
        backgroundColor = initBackgroundColor;
	mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
	Pane canvas = workspace.getCanvas();
	BackgroundFill fill = new BackgroundFill(backgroundColor, null, null);
	Background background = new Background(fill);
	canvas.setBackground(background);
    }
    
    public void setBackgroundImage(Image initBackgroundImage, String imgPath) {
        usingBGImage = true;
        backgroundImage = initBackgroundImage;
        backgroundImagePath = imgPath;
        BackgroundImage myBI= new BackgroundImage(initBackgroundImage,
        BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,BackgroundSize.DEFAULT);
        //then you set to your node
        ((mmmWorkspace)app.getWorkspaceComponent()).getCanvas().setBackground(new Background(myBI));
    }
    
    public void setCurrentOutlineColor(Color initColor) {
	currentOutlineColor = initColor;
	if (selectedNode != null) {
	    ((Shape)selectedNode).setStroke(initColor);
	}
    }
    public void setCurrentOutlineThickness(int initBorderWidth) {
	currentBorderWidth = initBorderWidth;
	if (selectedNode != null) {
	    ((Shape)selectedNode).setStrokeWidth(initBorderWidth);
	}
    }
    public mmmState getState() {
	return state;
    }
    public void setState(mmmState initState) {
        state = initState;
    }

    public DraggableStation findStation(String s) {
        for (int i = nodes.size() - 1; i >= 0; i--) {
	    if ((nodes.get(i) instanceof DraggableStation)&&((((DraggableStation)nodes.get(i)).toString()).equals(s))) {
		return (DraggableStation)nodes.get(i);
	    }
	}
        return null;
    }

    public DraggableLabel findLabel(String s) {
        System.out.println(s);
         for (int i = nodes.size() - 1; i >= 0; i--) {
	    if ((nodes.get(i) instanceof DraggableLabel)&&((((DraggableLabel)nodes.get(i)).toString()).equals(s))) {
                return (DraggableLabel)nodes.get(i);
	    }
	}
        System.out.println("Why");
        return null;
    }
    public ArrayList getStations(){
        return stations;
    }
    public ArrayList getLines(){
        return lines;
    }
    public void setFileName(String file){
        fileName = file;
        //((mmmWorkspace)app.getWorkspaceComponent()).setFileName(file);
    }
    public String getFileName(){
        return fileName;
    }
    public AppTemplate getApp(){
        return app;
    }
}
