/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import mmm.gui.CanvasController;

/**
 *
 * @author shawn
 */
public class DraggableStation extends Ellipse implements Draggable{
    double startCenterX;
    double startCenterY;
    double textX;
    double textY;
    String fontFamily;
    double fontSize;
    boolean bold;
    boolean italicize;
    Text name;
    int pos;
    int rot;
    
    public DraggableStation(){
        setCenterX(0.0);
	setCenterY(0.0);
	setRadiusX(10.0);
	setRadiusY(10.0);
	setOpacity(1.0);
        setStroke(Color.BLACK);
        setStrokeWidth(5);
	startCenterX = 0.0;
	startCenterY = 0.0;
        fontFamily = "Arial,Helvetica,sans-serif";
        fontSize = 16;
        bold = false;
        italicize = false;
        name = new Text("Untitled Station");
        name.setFont(Font.font(fontFamily,fontSize));
        name.setX(getCenterX()+getRadiusX());
        name.setY(getCenterY()-getRadiusX()-20);
        pos = 0;
        rot = 0;
    }
    
    public DraggableStation(String Name) {
	setCenterX(0.0);
	setCenterY(0.0);
	setRadiusX(10.0);
	setRadiusY(10.0);
	setOpacity(1.0);
        setStroke(Color.BLACK);
        setStrokeWidth(5);
	startCenterX = 0.0;
	startCenterY = 0.0;
        name = new Text(Name);
        name.setFont(Font.font("Arial,Helvetica,sans-serif",16));
        name.setX(getCenterX()+getRadiusX());
        name.setY(getCenterY()-getRadiusX()-20);
        fontFamily = "Arial,Helvetica,sans-serif";
        fontSize = 16;
        bold = false;
        italicize = false;
        pos = -1;
        rot = 0;
    }
    
    @Override
    public mmmState getStartingState() {
        return mmmState.STARTING_STATION;
    }

    @Override
    public void start(int x, int y) {
        startCenterX = x;
	startCenterY = y;
    }

    @Override
    public void drag(int x, int y) {
        double diffX = x - startCenterX - CanvasController.getInitialX();
	double diffY = y - startCenterY - CanvasController.getInitialY();
	double newX = startCenterX + diffX;
	double newY = startCenterY + diffY;
        if (newX - getRadiusX()> 0 && newX + getRadiusX()< 1560 && newY - getRadiusY()> 0 && newY + getRadiusY() < 900){
            centerXProperty().set(newX);
            centerYProperty().set(newY);
            startCenterX = newX;
            startCenterY = newY;
            if (pos == 0){
                name.setX(getCenterX()+getRadiusX());
                name.setY(getCenterY()-getRadiusY());
            } else if (pos == 1){
                name.setX(getCenterX()+getRadiusX());
                name.setY(getCenterY()+getRadiusY()+name.getBoundsInLocal().getHeight()/2);
            } else if (pos == 2){
                name.setX(getCenterX()-getRadiusX()-name.getBoundsInLocal().getWidth());
                name.setY(getCenterY()+getRadiusY()+name.getBoundsInLocal().getHeight()/2);
            } else {
                name.setX(getCenterX()-getRadiusX()-name.getBoundsInLocal().getWidth());
                name.setY(getCenterY()-getRadiusY());
            }
        }
    }

    @Override
    public void size(int x, int y) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getX() {
        return getCenterX();
    }

    @Override
    public double getY() {
        return getCenterY();
    }
    
    public void setStationX(double x){
        setCenterX(x);
        moveLabel();
    }
    
    public void setStationY(double y){
        setCenterY(y);
        moveLabel();
    }
    
    @Override
    public double getWidth() {
        return getRadiusX() * 2;
    }

    @Override
    public double getHeight() {
        return getRadiusY() * 2;
    }

    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
        setCenterX(initX + (initWidth/2));
	setCenterY(initY + (initHeight/2));
	setRadiusX(initWidth/2);
	setRadiusY(initHeight/2);
    }

    @Override
    public String getShapeType() {
        return STATION;
    }
    
    public void moveLabel(){
        if (pos == 0){
            name.setX(getCenterX()+getRadiusX());
            name.setY(getCenterY()+getRadiusY()+name.getBoundsInLocal().getHeight()/2);
            pos = 1;
        } else if (pos == 1){
            name.setX(getCenterX()-getRadiusX()-name.getBoundsInLocal().getWidth());
            name.setY(getCenterY()+getRadiusY()+name.getBoundsInLocal().getHeight()/2);
            pos = 2;
        } else if (pos == 2){
            name.setX(getCenterX()-getRadiusX()-name.getBoundsInLocal().getWidth());
            name.setY(getCenterY()-getRadiusY());
            pos = 3;
        } else {
            name.setX(getCenterX()+getRadiusX());
            name.setY(getCenterY()-getRadiusY());
            pos = 0;
        }
    }
    
    public void setPos(int i){
        if (i == 0){
            name.setX(getCenterX()+getRadiusX());
            name.setY(getCenterY()-getRadiusY());
            pos = 0;
        } else if (i == 1){
            name.setX(getCenterX()+getRadiusX());
            name.setY(getCenterY()+getRadiusY()+name.getBoundsInLocal().getHeight()/2);
            pos = 1;
        } else if (i == 2){
            name.setX(getCenterX()-getRadiusX()-name.getBoundsInLocal().getWidth());
            name.setY(getCenterY()+getRadiusY()+name.getBoundsInLocal().getHeight()/2);
            pos = 2;
        } else {
            name.setX(getCenterX()-getRadiusX()-name.getBoundsInLocal().getWidth());
            name.setY(getCenterY()-getRadiusY());
            pos = 3;
        }
    }
    
    public void setRot(int i){
        rot = i;
        name.setRotate(i);
    }
    
    public void rotate(){
        rot = rot+90;
        name.setRotate(rot);
    }
    
    public DraggableStation deepCopy(double x, double y){
        DraggableStation copy = new DraggableStation(name.toString());
        return copy;
    }
    
    public Text getName(){
        return name;
    }
    
    public void setName(String newName){
        name.setText(newName);
    }
    @Override
    public String toString(){
        return name.getText();
    }
    public int getPos(){
        return pos;
    }
    public int getRot(){
        return rot;
    }
    public double getFontSize(){
        return fontSize;
    }
    public String getFontFamily(){
        return fontFamily;
    }
    public boolean isBold(){
        return bold;
    }
    public boolean isItalic(){
        return italicize;
    }
    public void setFontSize(double size){
        fontSize = size;
    }
    public void setFontFamily(String family){
        fontFamily = family;
    }
    public void toggleBold(){
        bold = !bold;
    }
    public void toggleItalic(){
        italicize = !italicize;
    }
    public void changeFont(String fontFamily, double size, boolean bold, boolean italics){
        setFontFamily(fontFamily);
        setFontSize(size);
        this.bold = bold;
        this.italicize = italics;
        if (bold && italics){
            name.setFont(Font.font(fontFamily, FontWeight.BOLD, FontPosture.ITALIC, size));
        } else if (bold && !italics){
            name.setFont(Font.font(fontFamily, FontWeight.BOLD, size));
        } else if (!bold && italics){
            name.setFont(Font.font(fontFamily, FontPosture.ITALIC, size));
        } else {
           name.setFont(Font.font(fontFamily, size));
        }
        setPos(pos);
    }
    public void setBold(boolean b){
        bold = b;
    }
    public void setItalics(boolean i){
        italicize = i;
    }
}
