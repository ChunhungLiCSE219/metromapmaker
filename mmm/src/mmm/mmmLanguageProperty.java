/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm;

/**
 *
 * @author shawn
 */
public enum mmmLanguageProperty {
    SAVEAS_ICON,
    SAVEAS_TOOLTIP,
    EXPORT_ICON,
    EXPORT_TOOLTIP,
    
    UNDO_ICON,
    UNDO_TOOLTIP,
    REDO_ICON,
    REDO_TOOLTIP,
    
    ABOUT_ICON,
    ABOUT_TOOLTIP,
    
    LISTLINES_ICON,
    LISTLINES_TOOLTIP,
    ROUTE_ICON,
    ROUTE_TOOLTIP,
    
    ZOOMIN_ICON,
    ZOOMIN_TOOLTIP,
    ZOOMOUT_ICON,
    ZOOMOUT_TOOLTIP,
    SHRINK_ICON,
    SHRINK_TOOLTIP,
    GROW_ICON,
    GROW_TOOLTIP,
    
    LOGO_ICON,
}
