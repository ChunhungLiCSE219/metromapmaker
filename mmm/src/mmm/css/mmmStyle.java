/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.css;

/**
 *
 * @author shawn
 */
public class mmmStyle {
    public static final String CLASS_BORDERED_PANE = "bordered_pane";
    public static final String CLASS_MAX_PANE = "max_pane";
    public static final String CLASS_RENDER_CANVAS = "render_canvas";
    public static final String CLASS_BUTTON = "button";
    public static final String CLASS_BAR_BUTTON = "bar_button";
    public static final String CLASS_COMBOBOX = "combobox";
    public static final String CLASS_SLIDER = "slider";
    public static final String CLASS_EDIT_TOOLBAR = "edit_toolbar";
    public static final String CLASS_EDIT_TOOLBAR_ROW = "edit_toolbar_row";
    public static final String CLASS_ROW_HORIZONTAL_COMPONENT = "row_horizontal_component";
    public static final String CLASS_ROW_VERTICAL_COMPONENT = "row_vertical_component";
    public static final String CLASS_COLOR_CHOOSER_PANE = "color_chooser_pane";
    public static final String CLASS_COLOR_CHOOSER_CONTROL = "color_chooser_control";
    public static final String CLASS_WELCOME_COL_1 = "welcome_col-1";
    public static final String CLASS_WELCOME_COL_2 = "welcome_col-2";
    public static final String EMPTY_TEXT = "";
}
