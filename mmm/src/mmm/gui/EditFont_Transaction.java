/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import djf.AppTemplate;
import javafx.scene.text.Text;
import jtps.jTPS_Transaction;
import mmm.data.DraggableLabel;
import mmm.data.DraggableStation;
import mmm.data.mmmData;

/**
 *
 * @author shawn
 */
public class EditFont_Transaction implements jTPS_Transaction{
    
    mmmData dataManager;
    boolean oldIsBold;
    boolean oldIsItalic;
    double oldFontSize;
    String oldFontFamily;
    
    boolean newIsBold;
    boolean newIsItalic;
    double newFontSize;
    String newFontFamily;
    DraggableStation station;
    DraggableLabel label;
    
    public EditFont_Transaction(String fontFamily, double fontSize, boolean isBold, boolean isItalic, AppTemplate app){
        dataManager = (mmmData)app.getDataComponent();
        if (dataManager.getSelectedNode() instanceof DraggableLabel){
            label = (DraggableLabel)dataManager.getSelectedNode();
            oldIsBold = label.isBold();
            oldIsItalic = label.isItalic();
            oldFontSize = label.getFontSize();
            oldFontFamily = label.getFontFamily();
        } else {
            station = (DraggableStation)dataManager.getSelectedNode();
            oldIsBold = station.isBold();
            oldIsItalic = station.isItalic();
            oldFontSize = station.getFontSize();
            oldFontFamily = station.getFontFamily();
        }
        newIsBold = isBold;
        newIsItalic = isItalic;
        newFontSize = fontSize;
        newFontFamily = fontFamily; 
    }
    
    @Override
    public void doTransaction() {
         if (label != null){
             label.setBold(newIsBold);
             label.setItalics(newIsItalic);
             label.setFontSize(newFontSize);
             label.setFontFamily(newFontFamily);
             label.changeFont(newFontFamily, newFontSize, newIsBold, newIsItalic);
         } else {
             station.setBold(newIsBold);
             station.setItalics(newIsItalic);
             station.setFontSize(newFontSize);
             station.setFontFamily(newFontFamily);
             station.changeFont(newFontFamily, newFontSize, newIsBold, newIsItalic);
         }
    }

    @Override
    public void undoTransaction() {
        if (label != null){
             label.setBold(oldIsBold);
             label.setItalics(oldIsItalic);
             label.setFontSize(oldFontSize);
             label.setFontFamily(oldFontFamily);
             label.changeFont(oldFontFamily, oldFontSize, oldIsBold, oldIsItalic);
         } else {
             station.setBold(oldIsBold);
             station.setItalics(oldIsItalic);
             station.setFontSize(oldFontSize);
             station.setFontFamily(oldFontFamily);
             station.changeFont(oldFontFamily, oldFontSize, oldIsBold, oldIsItalic);
         }
    }
    
}
