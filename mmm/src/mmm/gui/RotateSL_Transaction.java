/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import djf.AppTemplate;
import jtps.jTPS_Transaction;
import mmm.data.DraggableStation;
import mmm.data.mmmData;

/**
 *
 * @author shawn
 */
public class RotateSL_Transaction implements jTPS_Transaction{
    
    DraggableStation station;
    
    public RotateSL_Transaction(AppTemplate app){
        mmmData dataManager = (mmmData)app.getDataComponent();
        station = (DraggableStation)dataManager.getSelectedNode();
    }
    
    @Override
    public void doTransaction() {
        station.rotate();
    }

    @Override
    public void undoTransaction() {
        station.rotate();
        station.rotate();
        station.rotate();
    }
    
}
