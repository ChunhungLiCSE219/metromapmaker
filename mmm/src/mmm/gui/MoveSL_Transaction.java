/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import djf.AppTemplate;
import jtps.jTPS_Transaction;
import mmm.data.DraggableStation;
import mmm.data.mmmData;

/**
 *
 * @author shawn
 */
public class MoveSL_Transaction implements jTPS_Transaction{

    DraggableStation station;
    int oldPos;
    
    public MoveSL_Transaction(AppTemplate app){
        mmmData dataManager = (mmmData)app.getDataComponent();
        station = (DraggableStation)dataManager.getSelectedNode();
        oldPos = station.getPos();
    }
    @Override
    public void doTransaction() {
        if (oldPos >= 3){
            station.setPos(0);
        } else {
            station.setPos(oldPos + 1);
        }
    }

    @Override
    public void undoTransaction() {
        station.setPos(oldPos);
    }
    
}
