/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import djf.AppTemplate;
import jtps.jTPS_Transaction;
import mmm.data.DraggableLabel;
import mmm.data.DraggableLineEnd;
import mmm.data.mmmData;

/**
 *
 * @author shawn
 */
public class ChangeLT_Transaction implements jTPS_Transaction{
    double oldThick;
    double newThick;
    DraggableLineEnd line;
    
    public ChangeLT_Transaction(double newT, AppTemplate app){
        mmmData dataManager = (mmmData)app.getDataComponent();
        line = ((DraggableLabel)(dataManager.getSelectedNode())).getLine();
        oldThick = line.getLineWidth();
        newThick = newT;
    }
    @Override
    public void doTransaction() {
        line.changeLineThick(newThick);
    }

    @Override
    public void undoTransaction() {
        line.changeLineThick(oldThick);
    }
    
    public void setLT(double newerThick){
        newThick = newerThick;
    }
}
