/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import djf.AppTemplate;
import javafx.scene.shape.Line;
import jtps.jTPS_Transaction;
import mmm.data.DraggableLabel;
import mmm.data.DraggableLineEnd;
import mmm.data.mmmData;

/**
 *
 * @author shawn
 */
public class RemoveLine_Transaction implements jTPS_Transaction{

    DraggableLineEnd line;
    mmmData dataManager;
    mmmWorkspace workspace;
    
    public RemoveLine_Transaction(DraggableLabel label, AppTemplate app){
        dataManager = (mmmData)app.getDataComponent();
        workspace = (mmmWorkspace)app.getWorkspaceComponent();
        line = ((DraggableLabel)(dataManager.getSelectedNode())).getLine();
    }
    
    @Override
    public void doTransaction() {
        for (int i = 0;i<=workspace.getLines().size()-1;i++){
            String s = (String)workspace.getLines().get(i);
            if (s.equals(line.getName())){
                workspace.getLines().remove(s);
                i--;
            }
        }
        dataManager.removeNode(line.getBeginning());
        dataManager.removeNode(line.getEnding());
        for (int i = 0; i < line.getConnectors().size(); i++){
            dataManager.removeNode(line.getConnectors().get(i));
        }
        dataManager.getLines().remove(line);
        dataManager.setSelectedNode(null);
    }

    @Override
    public void undoTransaction() {
        dataManager.setSelectedNode(line.getBeginning());
        dataManager.getLines().add(line);
        dataManager.addNode(line.getBeginning());
        dataManager.addNode(line.getEnding());
        for (int i = 0; i < line.getConnectors().size(); i++){
            dataManager.addNode(line.getConnectors().get(i));
        }
        workspace.getLines().add(line);
        workspace.getLinesBox().setValue(line.toString());
    }
    
}
