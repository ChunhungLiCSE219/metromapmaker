/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import djf.AppTemplate;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextInputControl;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.SnapshotParameters;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.image.WritableImage;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.imageio.ImageIO;
import mmm.data.Draggable;
import mmm.data.mmmData;
import mmm.data.mmmState;
import mmm.data.DraggableStation;
import mmm.data.DraggableImageView;
import mmm.data.DraggableLineEnd;
import mmm.data.DraggableLabel;
import mmm.gui.mmmWorkspace;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.ListenableUndirectedGraph;

/**
 *
 * @author shawn
 */
public class MapEditController {
    AppTemplate app;
    mmmData dataManager;
    Node clipboard;
    public MapEditController(AppTemplate initApp) {
	app = initApp;
	dataManager = (mmmData)app.getDataComponent();
    }
    public void processSelectSelectionTool(){
        
    }
    public void processRemoveSelected(){
        if ((dataManager.getSelectedNode() instanceof DraggableLabel && !((DraggableLabel)(dataManager.getSelectedNode())).isLineEnd())||(dataManager.getSelectedNode() instanceof DraggableImageView)){
            ((mmmWorkspace)app.getWorkspaceComponent()).getStack().addTransaction(new RemoveMapElement_Transaction((Draggable)dataManager.getSelectedNode(),app));
            ((mmmWorkspace)app.getWorkspaceComponent()).addStack();
        }
    }
    public void processSelectOutlineColor(){
        
    }
    public void processSelectBackgroundColor(){
        Stage dialog = new Stage();
        dialog.initStyle(StageStyle.UTILITY);
        dialog.setTitle("Background Color");
        dialog.setWidth(354);
        dialog.setHeight(118);
        VBox bgColorPane = new VBox();
        bgColorPane.setAlignment(Pos.CENTER);
        bgColorPane.setSpacing(3);
        Scene scene = new Scene(bgColorPane);
        HBox setColor = new HBox();
        setColor.setAlignment(Pos.CENTER);
        Label colorLabel = new Label("Choose Background Color: ");
        ColorPicker bgColorPicker = new ColorPicker(Color.WHITE);
        setColor.getChildren().add(colorLabel);
        setColor.getChildren().add(bgColorPicker);
        bgColorPane.getChildren().add(setColor);
        HBox confirm = new HBox();
        confirm.setAlignment(Pos.CENTER);
        Button confirmLine = new Button("Set Background Color");
        confirmLine.setPrefWidth(332);
        confirmLine.setOnAction(e->{
            ((mmmWorkspace)app.getWorkspaceComponent()).getStack().addTransaction(new ChangeBGColor_Transaction(Color.web(bgColorPicker.getValue().toString().substring(2, 8)),app));
            ((mmmWorkspace)app.getWorkspaceComponent()).addStack();
           dialog.close();
        });
        confirm.getChildren().add(confirmLine);
        bgColorPane.getChildren().add(confirm);
        dialog.setScene(scene);
        dialog.showAndWait();
        
    }
    public void processSelectBackgroundImage(){
        File imgFile = openImage();
        try {
            if (imgFile != null) {
                //replace with transaction
                ((mmmWorkspace)app.getWorkspaceComponent()).getStack().addTransaction(new ChangeImageBG_Transaction(imgFile.toURI().toURL().toExternalForm(),app));
                ((mmmWorkspace)app.getWorkspaceComponent()).addStack();
            } 
        } catch (MalformedURLException e) {
            showError(e);
        }
    }
    
    public void processSelectFontColor(){
        Stage dialog = new Stage();
        dialog.initStyle(StageStyle.UTILITY);
        dialog.setTitle("Font Color");
        dialog.setWidth(306);
        dialog.setHeight(118);
        VBox fontColorPane = new VBox();
        fontColorPane.setAlignment(Pos.CENTER);
        fontColorPane.setSpacing(3);
        Scene scene = new Scene(fontColorPane);
        HBox setColor = new HBox();
        setColor.setAlignment(Pos.CENTER);
        Label colorLabel = new Label("Choose Font Color: ");
        ColorPicker bgColorPicker = new ColorPicker(Color.WHITE);
        setColor.getChildren().add(colorLabel);
        setColor.getChildren().add(bgColorPicker);
        fontColorPane.getChildren().add(setColor);
        HBox confirm = new HBox();
        confirm.setAlignment(Pos.CENTER);
        Button confirmLine = new Button("Set Font Color");
        confirmLine.setPrefWidth(284);
        confirmLine.setOnAction(e->{
            //replace with transaction
            ((mmmWorkspace)app.getWorkspaceComponent()).getStack().addTransaction(new ChangeFFC_Transaction(bgColorPicker.getValue(),app));
            ((mmmWorkspace)app.getWorkspaceComponent()).addStack();
            dialog.close();
        });
        confirm.getChildren().add(confirmLine);
        fontColorPane.getChildren().add(confirm);
        dialog.setScene(scene);
        dialog.showAndWait();
    }
    public void processSelectFillColor(){
        
    }
    public void processSelectOutlineThickness(double d){
        if (dataManager.getSelectedNode() instanceof DraggableLabel && 
                ((DraggableLabel)dataManager.getSelectedNode()).isLineEnd()){
            ((mmmWorkspace)app.getWorkspaceComponent()).getStack().addTransaction(new ChangeLT_Transaction(d,app));
            ((mmmWorkspace)app.getWorkspaceComponent()).addStack();
        }
    }
    public void processRadiusSize(double i){
        if (dataManager.getSelectedNode() instanceof DraggableStation){
            //replace with transaction
            ((mmmWorkspace)app.getWorkspaceComponent()).getStack().addTransaction(new ChangeRadius_Transaction(i,app));
            ((mmmWorkspace)app.getWorkspaceComponent()).addStack();
        }
    }
    public void processSnapShot(){
	mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
	Pane canvas = workspace.getCanvas();
	WritableImage image = canvas.snapshot(new SnapshotParameters(), null);
	File file = new File("./export/" + workspace.getFileName() + "/" + workspace.getFileName() + " Metro.png");
	try {
	    ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Export Map");
            alert.setHeaderText("");
            alert.setContentText("Map Successfully Exported");

            alert.showAndWait();
	}
	catch(IOException ioe) {
	    ioe.printStackTrace();
    }
        
    }
    public void processEmbedImage(){
        File imgFile = openImage();
        try {
            if (imgFile != null) {
                displayImage(imgFile);
            } 
        } catch (MalformedURLException e) {
            showError(e);
        }
    }
    private File openImage(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select an Image File");
        fileChooser.getExtensionFilters().add(new ExtensionFilter(
            "Image Files", "*.png", "*.jpg", "*.gif"));
        return fileChooser.showOpenDialog(null);
    }
    void displayImage(File imgFile) throws MalformedURLException{
        Image node = new Image(imgFile.toURI().toURL().toExternalForm());
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
        workspace.getStack().addTransaction(new AddImageOverLay_Transaction(node,imgFile.toURI().toURL().toExternalForm(),app));
        workspace.addStack();
        app.getGUI().updateToolbarControls(false);
    }
    private void showError(Exception e){
        Alert info = new Alert(AlertType.ERROR);
        info.setTitle("Image not found");
        info.setHeaderText("Image not found");
        info.setContentText(e.getMessage());
        info.showAndWait();        
    }
    public void processEmbedText(){
        
    }
    public void processAbout(){
        
    }
    public void processCopy(){
        
    }
    public void processCut(){
        
    }
    public void processPaste(){
        
    }
    public void processChangeMode(mmmState state){
        if (state.equals(mmmState.ADD_STATION)){
            dataManager.setState(mmmState.ADD_STATION);
            app.getGUI().getPrimaryScene().setCursor(Cursor.CROSSHAIR);
        }
        if (state.equals(mmmState.REMOVE_STATION)){
            dataManager.setState(mmmState.REMOVE_STATION);
            app.getGUI().getPrimaryScene().setCursor(Cursor.CROSSHAIR);
        }
    }
    public void processAddStationToLine(DraggableStation selectedStation,DraggableLineEnd selectedLine){
        if (!selectedLine.getChildren().contains(selectedStation)){
            double stationX = selectedStation.getCenterX();
            double stationY = selectedStation.getCenterY();
            DraggableLabel begin = selectedLine.getBeginning();
            DraggableLabel end = selectedLine.getEnding();
            if (selectedLine.getChildren().size() == 0){
                ((Line)(selectedLine.getConnectors().get(0))).endXProperty().bind(selectedStation.centerXProperty());
                ((Line)(selectedLine.getConnectors().get(0))).endYProperty().bind(selectedStation.centerYProperty());
                Line newLine = selectedLine.formLine(selectedStation, end, selectedLine.getFillLine());
                (selectedLine.getConnectors()).add(newLine);
                dataManager.getNodes().add(0,newLine);
                selectedLine.getChildren().add(selectedStation);
            } else if (selectedLine.getChildren().size() == 1){
                double beginStartX = ((Line)(selectedLine.getConnectors().get(0))).getStartX();
                double beginStartY = ((Line)(selectedLine.getConnectors().get(0))).getStartY();
                double endEndX = ((Line)(selectedLine.getConnectors().get(1))).getEndX();
                double endEndY = ((Line)(selectedLine.getConnectors().get(1))).getEndY();
            
                double distanceBegin = distance(beginStartX,beginStartY,stationX,stationY);
                double distanceEnd = distance(stationX,stationY,endEndX,endEndY);
                if (distanceBegin < distanceEnd){
                    ((Line)(selectedLine.getConnectors().get(0))).endXProperty().bind(selectedStation.centerXProperty());
                    ((Line)(selectedLine.getConnectors().get(0))).endYProperty().bind(selectedStation.centerYProperty());
                    Line newLine = selectedLine.formLine(selectedStation, (DraggableStation)(selectedLine.getChildren().get(0)), selectedLine.getFillLine());
                    selectedLine.getConnectors().add(1,newLine);
                    dataManager.getNodes().add(0,newLine);
                    selectedLine.getChildren().add(0,selectedStation);
                } else {
                    ((Line)(selectedLine.getConnectors().get(1))).startXProperty().bind(selectedStation.centerXProperty());
                    ((Line)(selectedLine.getConnectors().get(1))).startYProperty().bind(selectedStation.centerYProperty());
                    Line newLine = selectedLine.formLine((DraggableStation)(selectedLine.getChildren().get(0)),selectedStation, selectedLine.getFillLine());
                    selectedLine.getConnectors().add(1,newLine);
                    dataManager.getNodes().add(0,newLine);
                    selectedLine.getChildren().add(1,selectedStation);
                }
            } else {
                int stationIndex = -1;
                double distanceStation = Double.MAX_VALUE;
                for (int i = 0; i < selectedLine.getChildren().size(); i++){
                    DraggableStation currentStation = (DraggableStation)selectedLine.getChildren().get(i);
                    double tempDistanceStation = distance(currentStation.getCenterX(),currentStation.getCenterY(),stationX,stationY); 
                    if (tempDistanceStation < distanceStation){
                        distanceStation = tempDistanceStation;
                        stationIndex = i;
                    }
                }
                double beginStartX = ((Line)(selectedLine.getConnectors().get(stationIndex))).getStartX();
                double beginStartY = ((Line)(selectedLine.getConnectors().get(stationIndex))).getStartY();
                double endEndX = ((Line)(selectedLine.getConnectors().get(stationIndex + 1))).getEndX();
                double endEndY = ((Line)(selectedLine.getConnectors().get(stationIndex + 1))).getEndY();
                double distanceBegin = distance(beginStartX,beginStartY,stationX,stationY);
                double distanceEnd = distance(stationX,stationY,endEndX,endEndY);
                if (distanceBegin < distanceEnd){
                    ((Line)(selectedLine.getConnectors().get(stationIndex))).endXProperty().bind(selectedStation.centerXProperty());
                    ((Line)(selectedLine.getConnectors().get(stationIndex))).endYProperty().bind(selectedStation.centerYProperty());
                    Line newLine = selectedLine.formLine(selectedStation, (DraggableStation)(selectedLine.getChildren().get(stationIndex)), selectedLine.getFillLine());
                    selectedLine.getConnectors().add(stationIndex + 1,newLine);
                    dataManager.getNodes().add(0,newLine);
                    selectedLine.getChildren().add(stationIndex,selectedStation);
                } else {
                    ((Line)(selectedLine.getConnectors().get(stationIndex+1))).startXProperty().bind(selectedStation.centerXProperty());
                    ((Line)(selectedLine.getConnectors().get(stationIndex+1))).startYProperty().bind(selectedStation.centerYProperty());
                    Line newLine = selectedLine.formLine((DraggableStation)(selectedLine.getChildren().get(stationIndex)),selectedStation, selectedLine.getFillLine());
                    selectedLine.getConnectors().add(stationIndex + 1,newLine);
                    dataManager.getNodes().add(0,newLine);
                    selectedLine.getChildren().add(stationIndex + 1,selectedStation);
                }
            }
            
        }
        
    }
    public double distance(double startX, double startY, double endX, double endY){
        double deltaX = endX - startX;
        double deltaY = endY - startY;
        return Math.sqrt(deltaX*deltaX + deltaY*deltaY);
    }
    public void processListStation(){
        Stage dialog = new Stage();
        dialog.initStyle(StageStyle.UTILITY);
        dialog.setTitle("List of Stations: " + (String)((mmmWorkspace)(app.getWorkspaceComponent())).getLinesBox().getValue() + "Line");
        VBox pane = new VBox();
        Scene scene = new Scene(pane);
        ArrayList<DraggableStation> stations = ((DraggableLabel)(((mmmData)app.getDataComponent()).getSelectedNode())).getLine().getChildren();
        ListView<String> list = new ListView();
        ObservableList<String> items = FXCollections.observableArrayList();
        for (int i = 0; i < stations.size(); i++){
            items.add(stations.get(i).getName().getText());
        }
        list.setItems(items);
        System.out.println(items.size());
        pane.getChildren().add(list);
        dialog.setScene(scene);
        scene.getStylesheets().add(getClass().getResource("listView.css").toExternalForm());
        dialog.showAndWait();
    }
    public void processSnapToGrid(){
        ((mmmWorkspace)app.getWorkspaceComponent()).getStack().addTransaction(new SnapNode_Transaction(app));
        ((mmmWorkspace)app.getWorkspaceComponent()).addStack();
    }
    public void processNavigate(int direction){
        
    }
    public void processMoveStation(){
        
    }
    public void processRotateStationLabel(){
        if (dataManager.getSelectedNode() instanceof DraggableStation){
            ((mmmWorkspace)app.getWorkspaceComponent()).getStack().addTransaction(new RotateSL_Transaction(app));
            ((mmmWorkspace)app.getWorkspaceComponent()).addStack();
            //((DraggableStation)dataManager.getSelectedNode()).rotate();
        }
    }

    void processRemoveLine() {
        ((mmmWorkspace)app.getWorkspaceComponent()).getStack().addTransaction(new RemoveLine_Transaction((DraggableLabel)((mmmData)app.getDataComponent()).getSelectedNode(),app));
        ((mmmWorkspace)app.getWorkspaceComponent()).addStack();
        ((mmmWorkspace)app.getWorkspaceComponent()).updateLineEditToolBar(false);
        
    }

    void processRemoveStationFromLine(DraggableStation selectedStation,DraggableLineEnd selectedLine) {
        if (selectedLine.getChildren().contains(selectedStation)){
            int stationIndex = selectedLine.getChildren().indexOf(selectedStation);
            selectedLine.getChildren().remove(selectedStation);
            if (stationIndex == selectedLine.getChildren().size()){
                ((Line)(selectedLine.getConnectors().get(stationIndex))).endXProperty().bind((selectedLine.getEnding()).xProperty().add((selectedLine.getEnding()).getBoundsInLocal().getWidth()/2));
                ((Line)(selectedLine.getConnectors().get(stationIndex))).endYProperty().bind((selectedLine.getEnding()).yProperty().subtract((selectedLine.getEnding()).getBoundsInLocal().getHeight()/2));
                dataManager.getNodes().remove((Line)selectedLine.getConnectors().remove(stationIndex + 1));
            } else {
                ((Line)(selectedLine.getConnectors().get(stationIndex))).endXProperty().bind(((DraggableStation)(selectedLine.getChildren().get(stationIndex))).centerXProperty());
                ((Line)(selectedLine.getConnectors().get(stationIndex))).endYProperty().bind(((DraggableStation)(selectedLine.getChildren().get(stationIndex))).centerYProperty());
                dataManager.getNodes().remove((Line)selectedLine.getConnectors().remove(stationIndex + 1));
            }
        }
    }

    void processRemoveStation() {
        ((mmmWorkspace)app.getWorkspaceComponent()).getStack().addTransaction(new RemoveStation_Transaction((DraggableStation)dataManager.getSelectedNode(),app));
        ((mmmWorkspace)app.getWorkspaceComponent()).addStack();
        ((mmmWorkspace)app.getWorkspaceComponent()).updateStationEditToolBar(false);
    }

    void processMoveStationLabel() {
        if (dataManager.getSelectedNode() instanceof DraggableStation){
            ((mmmWorkspace)app.getWorkspaceComponent()).getStack().addTransaction(new MoveSL_Transaction(app));
            ((mmmWorkspace)app.getWorkspaceComponent()).addStack();
        }
    }

    void processSelectLine() {
        String selected = (String)((mmmWorkspace)app.getWorkspaceComponent()).getLinesBox().getValue();
        if (selected != null){
            System.out.println(selected);
            mmmData dataManager = (mmmData)app.getDataComponent();
            DraggableLabel selectedLabel = dataManager.findLabel(selected + " - END");
            if (dataManager.getSelectedNode()!=null){
                dataManager.unhighlightNode(dataManager.getSelectedNode());
            }
            dataManager.highlightNode(selectedLabel);
            dataManager.setSelectedNode(selectedLabel);
            ((mmmWorkspace)(app.getWorkspaceComponent())).updateLineEditToolBar(true);
        }
    }


    void processSelectStation() {
        String selected = (String)((mmmWorkspace)app.getWorkspaceComponent()).getStationsBox().getValue();
        if (selected != null){
            mmmData dataManager = (mmmData)app.getDataComponent();
            DraggableStation selectedStation = dataManager.findStation(selected);
            if (dataManager.getSelectedNode()!=null){
                dataManager.unhighlightNode(dataManager.getSelectedNode());
            }
            dataManager.highlightNode(selectedStation);
            dataManager.setSelectedNode(selectedStation);
            ((mmmWorkspace)(app.getWorkspaceComponent())).updateStationEditToolBar(true);
        }
    }

    void processShowGrid() {
        dataManager.toggleGrid();
    }

    void processFindRoute() {
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
        Stage dialog = new Stage();
        dialog.initStyle(StageStyle.UTILITY);
        dialog.setTitle("Find Route: "+ (String)workspace.getStationBox1().getValue() + " to "+(String)workspace.getStationBox2().getValue());
        dialog.setWidth(800);
        dialog.setHeight(600);
        HBox pane = new HBox();
        pane.setStyle("-fx-background-color: linear-gradient(to right, #c8e0cb, #d2eefb);");
        VBox leftPane = new VBox();
        leftPane.setStyle("-fx-padding: 20px 20px 20px 20px; -fx-pref-width: 396px; -fx-pref-height: 596px; -fx-border-width: 2px 1px 2px 2px; -fx-border-color: #0039a6");
        Text mapHeader = new Text("Navigating " + workspace.getFileName().toUpperCase());
        Text fromHeader = new Text("From: "+ ((String)workspace.getStationBox1().getValue()).toUpperCase()+ " Station" + "\n" +"To: "+ ((String)workspace.getStationBox2().getValue()).toUpperCase()+" Station");
        mapHeader.setFill(Color.WHITE);
        mapHeader.setFont(Font.font("Arial,Helvetica,sans-serif;",FontWeight.BOLD, FontPosture.REGULAR, 30));
        
        fromHeader.setFill(Color.WHITE);
        fromHeader.setFont(Font.font("Arial,Helvetica,sans-serif;",FontWeight.BOLD, FontPosture.REGULAR, 24));
        HBox middleTransition = new HBox();
        middleTransition.setStyle("-fx-pref-width: 370px;-fx-pref-height: 0px;-fx-border-width: 0px 0px 2px 0px;-fx-border-color: #0039a6;");
        HBox bottomTransition = new HBox();
        bottomTransition.setStyle("-fx-pref-width: 370px;-fx-pref-height: 30px;-fx-border-width: 2px 0px 0px 0px;-fx-border-color: #0039a6;");
        Image i = new Image("file:./images/MMMLogo.png");
        ImageView iView = new ImageView(i);
        iView.setFitWidth(360);
        iView.setFitHeight(360);
        leftPane.getChildren().add(mapHeader);
        leftPane.getChildren().add(middleTransition);
        leftPane.getChildren().add(fromHeader);
        leftPane.getChildren().add(bottomTransition);
        leftPane.getChildren().add(iView);
        VBox rightPane = new VBox();
        rightPane.setSpacing(20);
        rightPane.setStyle("-fx-padding: 20px 20px 20px 20px;-fx-pref-width: 396px; -fx-pref-height: 596px; -fx-border-width: 2px 2px 2px 1px; -fx-border-color: #0039a6");
        ListView<String> list = new ListView();
        ObservableList<String> items = FXCollections.observableArrayList();
        GraphPath g = graphToShortest(mapToGraph());
        List<DraggableStation> l = (List<DraggableStation>)g.getVertexList();
        for (DraggableStation s: l){
            items.add(s.getName().getText());
        }
        items.add("Approximate Travel Time: " + 3*l.size() + " minutes");
        list.setItems(items);
        Button closeDirections = new Button("CLOSE DIRECTIONS");
        closeDirections.setStyle("-fx-pref-width: 360px;"+
                            "-fx-pref-height: 100px;"+
                            "-fx-background-color: #0039a6;" +
                            "-fx-border-insets: 5px;" +
                            "-fx-background-insets: 5px;" +
                            "-fx-border-width: 5px;" +
                            "-fx-border-color: #FFFFFF;" +
                            "-fx-font-weight: bold;" +
                            "-fx-text-fill: #FFFFFF;");
        rightPane.getChildren().add(list);
        rightPane.getChildren().add(closeDirections);
        Scene scene = new Scene(pane);
        pane.getChildren().add(leftPane);
        pane.getChildren().add(rightPane);
        dialog.setScene(scene);
        closeDirections.setOnAction(e->{
            dialog.close();
        });
        dialog.showAndWait();
    }
    
    public ListenableUndirectedGraph mapToGraph(){
        mmmData dataManager = (mmmData)app.getDataComponent();
        ListenableUndirectedGraph<DraggableStation,DefaultEdge> g = new ListenableUndirectedGraph<DraggableStation,DefaultEdge>(DefaultEdge.class);
        for (int i = 0; i < dataManager.getLines().size(); i++){
            DraggableLineEnd l = (DraggableLineEnd)dataManager.getLines().get(i);
            for (int j = 0; j < l.getChildren().size(); j++){
                g.addVertex(l.getChildren().get(j));
                if (j > 0){
                    g.addEdge(l.getChildren().get(j-1), l.getChildren().get(j));
                }
            }
        }
        return g;
    }
    public GraphPath graphToShortest(ListenableUndirectedGraph g){
        mmmData dataManager = (mmmData)app.getDataComponent();
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
        String s1 = (String)workspace.getStationBox1().getValue();
        DraggableStation station1 = null;
        for (int i = 0; i < dataManager.getStations().size(); i++){
            if ((s1.equals(((DraggableStation)dataManager.getStations().get(i)).getName().getText()))){
                station1 = ((DraggableStation)dataManager.getStations().get(i));
            }
        }
        String s2 = (String)workspace.getStationBox2().getValue();
        DraggableStation station2 = null;
        for (int i = 0; i < dataManager.getStations().size(); i++){
            if ((s2.equals(((DraggableStation)dataManager.getStations().get(i)).getName().getText()))){
                station2 = ((DraggableStation)dataManager.getStations().get(i));
            }
        }
        return new DijkstraShortestPath(g).getPath(station1, station2);
    }
}
