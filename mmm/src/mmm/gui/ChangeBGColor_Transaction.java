/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import djf.AppTemplate;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;
import mmm.data.mmmData;

/**
 *
 * @author shawn
 */
public class ChangeBGColor_Transaction implements jTPS_Transaction{
    Color olBGColor;
    Color BGColor;
    mmmData dataManager;
    
    public ChangeBGColor_Transaction(Color newColor, AppTemplate app){
        dataManager = (mmmData)app.getDataComponent();
        olBGColor = dataManager.getBackgroundColor();
        BGColor = newColor;
    }
    
    @Override
    public void doTransaction() {
        dataManager.setBackgroundColor(BGColor);
        dataManager.addGrid();
        double y = (299 * BGColor.getRed() + 587 * BGColor.getGreen() + 114 * BGColor.getBlue()) / 1000;
        if (y >= 0.5){
            dataManager.setGridColor(Color.BLACK);
        }
        else {
            dataManager.setGridColor(Color.WHITE);
        }
    }

    @Override
    public void undoTransaction() {
        dataManager.setBackgroundColor(olBGColor);
        double y = (299 * olBGColor.getRed() + 587 * olBGColor.getGreen() + 114 * olBGColor.getBlue()) / 1000;
        if (y >= 0.5){
            dataManager.setGridColor(Color.BLACK);
        }
        else {
            dataManager.setGridColor(Color.WHITE);
        }
    }
    
}
