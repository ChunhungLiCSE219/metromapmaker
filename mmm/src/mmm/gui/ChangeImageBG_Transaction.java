/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import djf.AppTemplate;
import javafx.scene.image.Image;
import jtps.jTPS_Transaction;
import mmm.data.mmmData;

/**
 *
 * @author shawn
 */
public class ChangeImageBG_Transaction implements jTPS_Transaction{
    
    mmmData dataManager;
    String olImagePath;
    String newImagePath;
    
    public ChangeImageBG_Transaction(String imagePath, AppTemplate app){
        dataManager = (mmmData)app.getDataComponent();
        olImagePath = dataManager.getBackgroundImagePath();
        newImagePath = imagePath;
    }
    
    @Override
    public void doTransaction() {
        dataManager.setBackgroundImage(new Image(newImagePath), newImagePath);
    }

    @Override
    public void undoTransaction() {
        dataManager.setBackgroundImage(new Image(olImagePath), olImagePath);
    }
    
}
