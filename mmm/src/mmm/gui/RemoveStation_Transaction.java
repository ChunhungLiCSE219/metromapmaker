/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import djf.AppTemplate;
import jtps.jTPS_Transaction;
import mmm.data.DraggableStation;
import mmm.data.mmmData;

/**
 *
 * @author shawn
 */
public class RemoveStation_Transaction implements jTPS_Transaction{
    
    DraggableStation station;
    mmmData dataManager;
    mmmWorkspace workspace;
    
    public RemoveStation_Transaction(DraggableStation s, AppTemplate app){
        dataManager = (mmmData)app.getDataComponent();
        workspace = (mmmWorkspace)app.getWorkspaceComponent();
        station = s;
        
    }
    @Override
    public void doTransaction() {
        for (int i = 0;i<=workspace.getStations().size()-1;i++){
            String s = workspace.getStations().get(i).toString();
            if (s.equals(dataManager.getSelectedNode().toString())){
                workspace.getStations().remove(s);
            }
        }
        dataManager.getStations().remove(station);
        dataManager.removeNode(station);
        dataManager.removeNode(station.getName());
        dataManager.setSelectedNode(null);
    }

    @Override
    public void undoTransaction() {
        dataManager.setSelectedNode(station);
        dataManager.addNode(station);
        dataManager.addNode(station.getName());
        dataManager.getStations().add(station);
        workspace.getStations().add(station.toString());
        workspace.getStationsBox().setValue(station.toString());
    }
    
}
