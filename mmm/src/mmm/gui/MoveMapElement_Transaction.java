/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import djf.AppTemplate;
import jtps.jTPS_Transaction;
import mmm.data.Draggable;
import mmm.data.DraggableImageView;
import mmm.data.DraggableLabel;
import mmm.data.DraggableStation;
import mmm.data.mmmData;

/**
 *
 * @author shawn
 */
public class MoveMapElement_Transaction implements jTPS_Transaction{
    
    Draggable node;
    double oldX;
    double oldY;
    double newX;
    double newY;
    
    public MoveMapElement_Transaction(AppTemplate app){
        mmmData dataManager = (mmmData)app.getDataComponent();
        node = (Draggable)dataManager.getSelectedNode();
        oldX = node.getX();
        oldY = node.getY();
    }
    
    @Override
    public void doTransaction() {
        if (node instanceof DraggableLabel){
            ((DraggableLabel)node).setX(newX);
            ((DraggableLabel)node).setY(newY);
        } if (node instanceof DraggableImageView){
            ((DraggableImageView)node).setX(newX);
            ((DraggableImageView)node).setY(newY);
        } if (node instanceof DraggableStation){
            DraggableStation station = (DraggableStation)node;
            station.setCenterX(newX);
            station.setCenterY(newY);
            if (station.getPos() == 0){
                station.getName().setX(station.getCenterX()+station.getRadiusX());
                station.getName().setY(station.getCenterY()-station.getRadiusY());
            } else if (station.getPos() == 1){
                station.getName().setX(station.getCenterX()+station.getRadiusX());
                station.getName().setY(station.getCenterY()+station.getRadiusY()+station.getName().getBoundsInLocal().getHeight()/2);
            } else if (station.getPos() == 2){
                station.getName().setX(station.getCenterX()-station.getRadiusX()-station.getName().getBoundsInLocal().getWidth());
                station.getName().setY(station.getCenterY()+station.getRadiusY()+station.getName().getBoundsInLocal().getHeight()/2);
            } else {
                station.getName().setX(station.getCenterX()-station.getRadiusX()-station.getName().getBoundsInLocal().getWidth());
                station.getName().setY(station.getCenterY()-station.getRadiusY());
            }
            
        }
    }

    @Override
    public void undoTransaction() {
        if (node instanceof DraggableLabel){
            ((DraggableLabel)node).setX(oldX);
            ((DraggableLabel)node).setY(oldY);
        } if (node instanceof DraggableImageView){
            ((DraggableImageView)node).setX(oldX);
            ((DraggableImageView)node).setY(oldY);
        } if (node instanceof DraggableStation){
            DraggableStation station = (DraggableStation)node;
            station.setCenterX(oldX);
            station.setCenterY(oldY);
            if (station.getPos() == 0){
                station.getName().setX(station.getCenterX()+station.getRadiusX());
                station.getName().setY(station.getCenterY()-station.getRadiusY());
            } else if (station.getPos() == 1){
                station.getName().setX(station.getCenterX()+station.getRadiusX());
                station.getName().setY(station.getCenterY()+station.getRadiusY()+station.getName().getBoundsInLocal().getHeight()/2);
            } else if (station.getPos() == 2){
                station.getName().setX(station.getCenterX()-station.getRadiusX()-station.getName().getBoundsInLocal().getWidth());
                station.getName().setY(station.getCenterY()+station.getRadiusY()+station.getName().getBoundsInLocal().getHeight()/2);
            } else {
                station.getName().setX(station.getCenterX()-station.getRadiusX()-station.getName().getBoundsInLocal().getWidth());
                station.getName().setY(station.getCenterY()-station.getRadiusY());
            }
            
        }
    }
    public void setNewXY(double x, double y){
        newX = x;
        newY = y;
    }
    
}
