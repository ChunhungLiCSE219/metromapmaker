/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import djf.AppTemplate;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;
import mmm.data.DraggableStation;
import mmm.data.mmmData;

/**
 *
 * @author shawn
 */
public class ChangeSFC_Transaction implements jTPS_Transaction{
    
    Color oldColor;
    String oldText;
    Color newColor;
    String newText;
    DraggableStation station;
    AppTemplate app;
    
    public ChangeSFC_Transaction(Color newC, String newT, AppTemplate app){
        this.app = app;
        mmmData dataManager = (mmmData)app.getDataComponent();
        station = (DraggableStation)dataManager.getSelectedNode();
        oldColor = (Color) station.getFill();
        oldText = station.getName().getText();
        newColor = newC;
        newText = newT;
    }
    
    @Override
    public void doTransaction() {
        for (int i = 0;i<=((mmmWorkspace)app.getWorkspaceComponent()).getStations().size()-1;i++){
                String s = ((mmmWorkspace)app.getWorkspaceComponent()).getStations().get(i).toString();
                if (s.equals(oldText)){
                    ((mmmWorkspace)app.getWorkspaceComponent()).getStations().remove(s);
                }
            }
            ((mmmWorkspace)app.getWorkspaceComponent()).getStations().add(newText);
            ((mmmWorkspace)app.getWorkspaceComponent()).getStationsBox().setValue(newText);
            station.setFill(newColor);
            station.setName(newText);
    }

    @Override
    public void undoTransaction() {
        for (int i = 0;i<=((mmmWorkspace)app.getWorkspaceComponent()).getStations().size()-1;i++){
                String s = ((mmmWorkspace)app.getWorkspaceComponent()).getStations().get(i).toString();
                if (s.equals(newText)){
                    ((mmmWorkspace)app.getWorkspaceComponent()).getStations().remove(s);
                }
            }
            ((mmmWorkspace)app.getWorkspaceComponent()).getStations().add(oldText);
            ((mmmWorkspace)app.getWorkspaceComponent()).getStationsBox().setValue(oldText);
            station.setFill(oldColor);
            station.setName(oldText);

    }
    
}
