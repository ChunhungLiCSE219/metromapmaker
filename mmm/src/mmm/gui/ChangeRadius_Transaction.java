/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import djf.AppTemplate;
import jtps.jTPS_Transaction;
import mmm.data.DraggableStation;
import mmm.data.mmmData;

/**
 *
 * @author shawn
 */
public class ChangeRadius_Transaction implements jTPS_Transaction{
    
    double oldRadius;
    double newRadius;
    DraggableStation station;
    
    public ChangeRadius_Transaction(double newR, AppTemplate app){
        mmmData dataManager = (mmmData)app.getDataComponent();
        station = (DraggableStation)dataManager.getSelectedNode();
        oldRadius = station.getRadiusX();
        newRadius = newR;
    }

    
    @Override
    public void doTransaction() {
        station.setRadiusX(newRadius);
        station.setRadiusY(newRadius);
        if (station.getPos() == 0){
            station.getName().setX(station.getCenterX()+station.getRadiusX());
            station.getName().setY(station.getCenterY()-station.getRadiusY());
        } else if (station.getPos() == 1){
            station.getName().setX(station.getCenterX()+station.getRadiusX());
            station.getName().setY(station.getCenterY()+station.getRadiusY()+station.getName().getBoundsInLocal().getHeight()/2);
        } else if (station.getPos() == 2){
            station.getName().setX(station.getCenterX()-station.getRadiusX()-station.getName().getBoundsInLocal().getWidth());
            station.getName().setY(station.getCenterY()+station.getRadiusY()+station.getName().getBoundsInLocal().getHeight()/2);
        } else {
            station.getName().setX(station.getCenterX()-station.getRadiusX()-station.getName().getBoundsInLocal().getWidth());
            station.getName().setY(station.getCenterY()-station.getRadiusY());
        }
    }

    @Override
    public void undoTransaction() {
        station.setRadiusX(oldRadius);
        station.setRadiusY(oldRadius);
        if (station.getPos() == 0){
            station.getName().setX(station.getCenterX()+station.getRadiusX());
            station.getName().setY(station.getCenterY()-station.getRadiusY());
        } else if (station.getPos() == 1){
            station.getName().setX(station.getCenterX()+station.getRadiusX());
            station.getName().setY(station.getCenterY()+station.getRadiusY()+station.getName().getBoundsInLocal().getHeight()/2);
        } else if (station.getPos() == 2){
            station.getName().setX(station.getCenterX()-station.getRadiusX()-station.getName().getBoundsInLocal().getWidth());
            station.getName().setY(station.getCenterY()+station.getRadiusY()+station.getName().getBoundsInLocal().getHeight()/2);
        } else {
            station.getName().setX(station.getCenterX()-station.getRadiusX()-station.getName().getBoundsInLocal().getWidth());
            station.getName().setY(station.getCenterY()-station.getRadiusY());
        }
    }
    
    public void setRadius(double newerRadius){
        newRadius = newerRadius;
    }
    
}
