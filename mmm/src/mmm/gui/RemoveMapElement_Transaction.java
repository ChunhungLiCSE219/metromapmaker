/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import djf.AppTemplate;
import jtps.jTPS_Transaction;
import mmm.data.Draggable;
import mmm.data.DraggableImageView;
import mmm.data.DraggableLabel;
import mmm.data.mmmData;

/**
 *
 * @author shawn
 */
public class RemoveMapElement_Transaction implements jTPS_Transaction{
    
    DraggableLabel label;
    DraggableImageView imageView;
    mmmData dataManager;
    
    public RemoveMapElement_Transaction(Draggable node,AppTemplate app){
        dataManager = (mmmData)app.getDataComponent();
        if (node instanceof DraggableLabel){
            label = (DraggableLabel)node;
        } else {
            imageView = (DraggableImageView)node;
        }
    }
    
    @Override
    public void doTransaction() {
        if (label != null){
            dataManager.getNodes().remove(label);
        } else {
            dataManager.getNodes().remove(imageView);
        }
        dataManager.setSelectedNode(null);
    }

    @Override
    public void undoTransaction() {
        if (label != null){
            dataManager.addNode(label);
        } else {
            dataManager.addNode(imageView);
        }
    }
    
}
