/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import djf.AppTemplate;
import jtps.jTPS_Transaction;
import mmm.data.DraggableStation;
import mmm.data.mmmData;

/**
 *
 * @author shawn
 */
public class SnapNode_Transaction implements jTPS_Transaction{

    DraggableStation station;
    int oldX;
    int oldY;
    
    public SnapNode_Transaction(AppTemplate app){
        mmmData dataManager = (mmmData)app.getDataComponent();
        station = (DraggableStation)dataManager.getSelectedNode();
        oldX = (int)station.getCenterX();
        oldY = (int)station.getCenterY();
    }
    
    @Override
    public void doTransaction() {
        int x = ((int)(station.getCenterX() + 5) / 10) * 10;
        int y = ((int)(station.getCenterY() + 5) / 10) * 10;
        station.setCenterX(x);
        station.setCenterY(y);
        station.start(x,y);
        if (station.getPos() == 0){
                station.getName().setX(station.getCenterX()+station.getRadiusX());
                station.getName().setY(station.getCenterY()-station.getRadiusY());
            } else if (station.getPos() == 1){
                station.getName().setX(station.getCenterX()+station.getRadiusX());
                station.getName().setY(station.getCenterY()+station.getRadiusY()+station.getName().getBoundsInLocal().getHeight()/2);
            } else if (station.getPos() == 2){
                station.getName().setX(station.getCenterX()-station.getRadiusX()-station.getName().getBoundsInLocal().getWidth());
                station.getName().setY(station.getCenterY()+station.getRadiusY()+station.getName().getBoundsInLocal().getHeight()/2);
            } else {
                station.getName().setX(station.getCenterX()-station.getRadiusX()-station.getName().getBoundsInLocal().getWidth());
                station.getName().setY(station.getCenterY()-station.getRadiusY());
            }
    }

    @Override
    public void undoTransaction() {
        station.setCenterX(oldX);
        station.setCenterY(oldY);
        if (station.getPos() == 0){
                station.getName().setX(station.getCenterX()+station.getRadiusX());
                station.getName().setY(station.getCenterY()-station.getRadiusY());
            } else if (station.getPos() == 1){
                station.getName().setX(station.getCenterX()+station.getRadiusX());
                station.getName().setY(station.getCenterY()+station.getRadiusY()+station.getName().getBoundsInLocal().getHeight()/2);
            } else if (station.getPos() == 2){
                station.getName().setX(station.getCenterX()-station.getRadiusX()-station.getName().getBoundsInLocal().getWidth());
                station.getName().setY(station.getCenterY()+station.getRadiusY()+station.getName().getBoundsInLocal().getHeight()/2);
            } else {
                station.getName().setX(station.getCenterX()-station.getRadiusX()-station.getName().getBoundsInLocal().getWidth());
                station.getName().setY(station.getCenterY()-station.getRadiusY());
            }
    }
    
}
