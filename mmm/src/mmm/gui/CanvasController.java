/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import djf.AppTemplate;
import javafx.scene.control.TextInputControl;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.Scene;
import javafx.scene.Node;
import java.util.Optional;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import mmm.data.Draggable;
import mmm.data.DraggableStation;
import mmm.data.DraggableLineEnd;
import mmm.data.DraggableImageView;
import mmm.data.DraggableLabel;
import mmm.data.mmmData;
import mmm.data.mmmState;
import static mmm.data.mmmState.*;
/**
 *
 * @author shawn
 */
public class CanvasController {
    AppTemplate app;
    mmmWorkspace workspace;
    mmmData dataManager;
    static double initialx;
    static double initialy;
    MoveMapElement_Transaction move;
    public CanvasController(AppTemplate initApp) {
        app = initApp;
        workspace = (mmmWorkspace)app.getWorkspaceComponent();
        dataManager = (mmmData)app.getDataComponent();
    }
    public void processCanvasMousePress(double x, double y){
        dataManager = (mmmData)app.getDataComponent();
        if (dataManager.isInState(SELECTING_SHAPE)){
            Node shape = dataManager.selectTopNode(x,y);
            Scene scene = app.getGUI().getPrimaryScene();
            if (shape != null){
                scene.setCursor(Cursor.MOVE);
                dataManager.setState(mmmState.DRAGGING_SHAPE);
                Draggable selectedDraggableShape = (Draggable) dataManager.getSelectedNode();
                if (selectedDraggableShape instanceof DraggableStation){
                    ((mmmWorkspace)app.getWorkspaceComponent()).getStationsBox().setValue(((DraggableStation) selectedDraggableShape).toString());
                }
                if (selectedDraggableShape instanceof DraggableLabel && ((DraggableLabel)selectedDraggableShape).isLineEnd()){
                    ((mmmWorkspace)app.getWorkspaceComponent()).getLinesBox().setValue(((DraggableLabel) selectedDraggableShape).toString().substring(0, ((DraggableLabel) selectedDraggableShape).toString().length()-6));
                }
                move = new MoveMapElement_Transaction(app);
                initialx = x - selectedDraggableShape.getX();
                initialy = y - selectedDraggableShape.getY();
                
                app.getWorkspaceComponent().reloadWorkspace(dataManager);
            } else {
                scene.setCursor(Cursor.DEFAULT);
                dataManager.setState(DRAGGING_NOTHING);
                app.getWorkspaceComponent().reloadWorkspace(dataManager);
            }
        }
        else if (dataManager.isInState(ADD_STATION)||dataManager.isInState(REMOVE_STATION)){
            Scene scene = app.getGUI().getPrimaryScene();
            DraggableLineEnd line = null;
            String s = ((mmmWorkspace)app.getWorkspaceComponent()).getLinesBox().getValue().toString();
            for (int i = 0; i<=((mmmData)app.getDataComponent()).getLines().size()-1;i++){
                if (s.equals(((DraggableLineEnd)((mmmData)app.getDataComponent()).getLines().get(i)).getName())){
                    line = (DraggableLineEnd)((mmmData)app.getDataComponent()).getLines().get(i);
                }
            }
            if (dataManager.getTopNode(x, y) instanceof DraggableStation){
                if (dataManager.isInState(ADD_STATION)){
                    workspace.getStack().addTransaction(new AddSTL_Transaction((DraggableStation)dataManager.getTopNode(x, y),line,app));
                    workspace.addStack();
                }
                if (dataManager.isInState(REMOVE_STATION)){
                    workspace.getStack().addTransaction(new RemoveSFL_Transaction((DraggableStation)dataManager.getTopNode(x, y),line,app));
                    workspace.addStack();
                }
            } else {
                scene.setCursor(Cursor.DEFAULT);
                dataManager.setState(DRAGGING_NOTHING);
                app.getWorkspaceComponent().reloadWorkspace(dataManager);
            }
        }
    }
    public void processCanvasMouseClick(double x, double y){
        
    }
    public void processCanvasMouseDragged(double x, double y){
        dataManager = (mmmData)app.getDataComponent();
        if (dataManager.isInState(DRAGGING_SHAPE)){
            if (dataManager.getSelectedNode() instanceof Draggable){
                Draggable selectedDraggableShape = (Draggable)dataManager.getSelectedNode();
                selectedDraggableShape.drag((int)x, (int)y);
            }
        }
        
    }
    public void processCanvasMouseRelease(double x, double y){
        dataManager = (mmmData)app.getDataComponent();
        workspace = (mmmWorkspace)app.getWorkspaceComponent();
        if (dataManager.isInState(DRAGGING_SHAPE)){
            dataManager.setState(SELECTING_SHAPE);
            move.setNewXY(((Draggable)dataManager.getSelectedNode()).getX(), ((Draggable)dataManager.getSelectedNode()).getY());
            workspace.getStack().addTransaction(move);
            workspace.addStack();
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.DEFAULT);
        } else if (dataManager.isInState(DRAGGING_NOTHING)){
            dataManager.setState(SELECTING_SHAPE);
        }
    }
    public void processModifyFont(int value){
        dataManager = (mmmData)app.getDataComponent();
        workspace = (mmmWorkspace)app.getWorkspaceComponent();
        if ((workspace.getFontFamilyBox().getValue()!=null && value == 0) || 
                (workspace.getFontSizeBox().getValue()!=null && value > 0) ||
                value == -1000 || value == -2000){
            if (dataManager.getSelectedNode() instanceof DraggableLabel){
                //replace with transaction
                DraggableLabel label = (DraggableLabel)dataManager.getSelectedNode();
                if (value == -1000){
                    workspace.getStack().addTransaction(new EditFont_Transaction(label.getFontFamily(),label.getFontSize(),!label.isBold(),label.isItalic(),app));
                } else if (value == -2000){
                    workspace.getStack().addTransaction(new EditFont_Transaction(label.getFontFamily(),label.getFontSize(),label.isBold(),!label.isItalic(),app));
                } else if (value == 0){
                    workspace.getStack().addTransaction(new EditFont_Transaction((String)((mmmWorkspace)(dataManager.getApp().getWorkspaceComponent())).getFontFamilyBox().getValue(),label.getFontSize(),label.isBold(),label.isItalic(),app));
                } else {
                    workspace.getStack().addTransaction(new EditFont_Transaction(label.getFontFamily(),(int)((mmmWorkspace)(dataManager.getApp().getWorkspaceComponent())).getFontSizeBox().getValue(),label.isBold(),label.isItalic(),app));
                }
                workspace.addStack();
            } else {
                //replace with transaction
                
                DraggableStation station = (DraggableStation)dataManager.getSelectedNode();
                if (value == -1000){
                    workspace.getStack().addTransaction(new EditFont_Transaction(station.getFontFamily(),station.getFontSize(),!station.isBold(),station.isItalic(),app));
                } else if (value == -2000){
                    workspace.getStack().addTransaction(new EditFont_Transaction(station.getFontFamily(),station.getFontSize(),station.isBold(),!station.isItalic(),app));
                } else if (value == 0){
                    workspace.getStack().addTransaction(new EditFont_Transaction((String)((mmmWorkspace)(dataManager.getApp().getWorkspaceComponent())).getFontFamilyBox().getValue(),station.getFontSize(),station.isBold(),station.isItalic(),app));
                } else {
                   workspace.getStack().addTransaction(new EditFont_Transaction(station.getFontFamily(),(int)((mmmWorkspace)(dataManager.getApp().getWorkspaceComponent())).getFontSizeBox().getValue(),station.isBold(),station.isItalic(),app));
                }
                workspace.addStack();    
            }
        }
    }
    public void processSelectFontColor(){
        
    }
    public void processResize(boolean grow){
        
    }
    public static double getInitialX(){
        return initialx;
    }
    public static double getInitialY(){
        return initialy;
    }

    public void processAddLine() {
        Stage dialog = new Stage();
        dialog.initStyle(StageStyle.UTILITY);
        dialog.setTitle("Add Line");
        dialog.setWidth(328);
        dialog.setHeight(150);
        VBox addLinePane = new VBox();
        addLinePane.setAlignment(Pos.CENTER);
        addLinePane.setSpacing(3);
        Scene scene = new Scene(addLinePane);
        HBox name = new HBox();
        name.setAlignment(Pos.CENTER);
        Label nameLabel = new Label("Enter Line Name: ");
        TextField textfield = new TextField();
        name.getChildren().add(nameLabel);
        name.getChildren().add(textfield);
        addLinePane.getChildren().add(name);
        HBox color = new HBox();
        color.setAlignment(Pos.CENTER);
        Label colorLabel = new Label("Select Line Color: ");
        ColorPicker LineColorPicker = new ColorPicker(Color.BLUE);
        LineColorPicker.prefWidthProperty().bind(textfield.widthProperty());
        color.getChildren().add(colorLabel);
        color.getChildren().add(LineColorPicker);
        addLinePane.getChildren().add(color);
        HBox confirm = new HBox();
        confirm.setAlignment(Pos.CENTER);
        Button confirmLine = new Button("Add Line");
        confirmLine.setPrefWidth(306);
        confirmLine.setOnAction(e->{
            dialog.close();
            boolean exists = false;
            //add what to do with result
            workspace = (mmmWorkspace)app.getWorkspaceComponent();
            for (int i = 0; i < workspace.getLines().size(); i++){
                if (textfield.getText().equals(workspace.getLines().get(i))){
                    exists = true;
                }
            }
            if (!exists){
                workspace.getStack().addTransaction(new AddLine_Transaction(LineColorPicker.getValue(),textfield.getText(),app));
                workspace.addStack();
                ((mmmWorkspace)app.getWorkspaceComponent()).updateLineEditToolBar(true);
            } else {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Duplicate Line Name");
                alert.setHeaderText(null);
                alert.setContentText("Line already exists, rename line");

                alert.showAndWait();
            }
        });
        confirm.getChildren().add(confirmLine);
        addLinePane.getChildren().add(confirm);
        dialog.setScene(scene);
        dialog.showAndWait();
    }
    
    void processEditLine(){
        Stage dialog = new Stage();
        dialog.initStyle(StageStyle.UTILITY);
        dialog.setTitle("Edit Line");
        dialog.setWidth(328);
        dialog.setHeight(150);
        VBox addLinePane = new VBox();
        addLinePane.setAlignment(Pos.CENTER);
        addLinePane.setSpacing(3);
        Scene scene = new Scene(addLinePane);
        HBox name = new HBox();
        name.setAlignment(Pos.CENTER);
        Label nameLabel = new Label("Enter Line Name: ");
        TextField textfield = new TextField();
        name.getChildren().add(nameLabel);
        name.getChildren().add(textfield);
        addLinePane.getChildren().add(name);
        HBox color = new HBox();
        color.setAlignment(Pos.CENTER);
        Label colorLabel = new Label("Select Line Color: ");
        ColorPicker LineColorPicker = new ColorPicker(Color.BLUE);
        LineColorPicker.prefWidthProperty().bind(textfield.widthProperty());
        color.getChildren().add(colorLabel);
        color.getChildren().add(LineColorPicker);
        addLinePane.getChildren().add(color);
        HBox confirm = new HBox();
        confirm.setAlignment(Pos.CENTER);
        Button confirmLine = new Button("Edit Line");
        confirmLine.setPrefWidth(306);
        confirmLine.setOnAction(e->{
            dialog.close();
            //add what to do with result
            ((mmmWorkspace)app.getWorkspaceComponent()).getStack().addTransaction(new EditLine_Transaction(LineColorPicker.getValue(),textfield.getText(),app));
            workspace.addStack();
        });
        confirm.getChildren().add(confirmLine);
        addLinePane.getChildren().add(confirm);
        dialog.setScene(scene);
        dialog.showAndWait();
    }
    
    void processAddStation() {
        Stage dialog = new Stage();
        dialog.initStyle(StageStyle.UTILITY);
        dialog.setTitle("Add Station");
        dialog.setWidth(350);
        dialog.setHeight(150);
        VBox addStationPane = new VBox();
        addStationPane.setAlignment(Pos.CENTER);
        addStationPane.setSpacing(3);
        Scene scene = new Scene(addStationPane);
        HBox name = new HBox();
        name.setAlignment(Pos.CENTER);
        Label nameLabel = new Label("Enter Station Name: ");
        TextField textfield = new TextField();
        name.getChildren().add(nameLabel);
        name.getChildren().add(textfield);
        addStationPane.getChildren().add(name);
        HBox color = new HBox();
        color.setAlignment(Pos.CENTER);
        Label colorLabel = new Label("Select Station Color: ");
        ColorPicker LineColorPicker = new ColorPicker(Color.WHITE);
        LineColorPicker.prefWidthProperty().bind(textfield.widthProperty());
        color.getChildren().add(colorLabel);
        color.getChildren().add(LineColorPicker);
        addStationPane.getChildren().add(color);
        HBox confirm = new HBox();
        confirm.setAlignment(Pos.CENTER);
        Button confirmLine = new Button("Add Station");
        confirmLine.setPrefWidth(328);
        confirmLine.setOnAction(e->{
            dialog.close();
            boolean exists = false;
            workspace = (mmmWorkspace)app.getWorkspaceComponent();
            for (int i = 0; i < workspace.getStations().size(); i++){
                if (textfield.getText().equals(workspace.getStations().get(i))){
                    exists = true;
                }
            }
            if (!exists){
                workspace.getStack().addTransaction(new AddStation_Transaction(LineColorPicker.getValue(),textfield.getText(),app));
                workspace.addStack();
                ((mmmWorkspace)app.getWorkspaceComponent()).updateStationEditToolBar(true);
            } else {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Duplicate Station Name");
                alert.setHeaderText(null);
                alert.setContentText("Station already exists, rename Station");

                alert.showAndWait();
            }
        });
        confirm.getChildren().add(confirmLine);
        addStationPane.getChildren().add(confirm);
        dialog.setScene(scene);
        dialog.showAndWait();
        System.out.println("why");
    }
    
    void processEditStation() {
        Stage dialog = new Stage();
        dialog.initStyle(StageStyle.UTILITY);
        dialog.setTitle("Edit Station");
        dialog.setWidth(350);
        dialog.setHeight(150);
        VBox addLinePane = new VBox();
        addLinePane.setAlignment(Pos.CENTER);
        addLinePane.setSpacing(3);
        Scene scene = new Scene(addLinePane);
        HBox name = new HBox();
        name.setAlignment(Pos.CENTER);
        Label nameLabel = new Label("Enter Station Name: ");
        TextField textfield = new TextField();
        name.getChildren().add(nameLabel);
        name.getChildren().add(textfield);
        addLinePane.getChildren().add(name);
        HBox color = new HBox();
        color.setAlignment(Pos.CENTER);
        Label colorLabel = new Label("Select Station Color: ");
        ColorPicker LineColorPicker = new ColorPicker(Color.BLUE);
        LineColorPicker.prefWidthProperty().bind(textfield.widthProperty());
        color.getChildren().add(colorLabel);
        color.getChildren().add(LineColorPicker);
        addLinePane.getChildren().add(color);
        HBox confirm = new HBox();
        confirm.setAlignment(Pos.CENTER);
        Button confirmLine = new Button("Edit Station");
        confirmLine.setPrefWidth(328);
        confirmLine.setOnAction(e->{
            //replace with transaction
            ((mmmWorkspace)app.getWorkspaceComponent()).getStack().addTransaction(new ChangeSFC_Transaction(LineColorPicker.getValue(),textfield.getText(),app));
            workspace.addStack();
            dialog.close();
        });
        confirm.getChildren().add(confirmLine);
        addLinePane.getChildren().add(confirm);
        dialog.setScene(scene);
        dialog.showAndWait();
        
    }
    
    void processAddLabel() {
        workspace = (mmmWorkspace)app.getWorkspaceComponent();
        TextInputDialog dialog = new TextInputDialog("Label");
        dialog.setTitle("Add Label");
        dialog.setHeaderText("");
        dialog.setContentText("Enter Label Name:");

        // Traditional way to get the response value.
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()){
            workspace.getStack().addTransaction(new AddLabel_Transaction(result.toString().substring(9,result.toString().length()-1),app));
            workspace.addStack();
        }

    }

    void processZoom(boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To body of generated methods, choose Tools | Templates.
    }

}
