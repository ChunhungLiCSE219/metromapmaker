/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import djf.AppTemplate;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;
import mmm.data.DraggableLabel;
import mmm.data.DraggableLineEnd;
import mmm.data.DraggableStation;
import mmm.data.mmmData;

/**
 *
 * @author shawn
 */
public class EditLine_Transaction implements jTPS_Transaction{
    
    Color oldColor;
    String oldName;
    Color newColor;
    String newName;
    DraggableLineEnd line;
    AppTemplate app;
    
    public EditLine_Transaction(Color newC, String newT, AppTemplate app){
        this.app = app;
        mmmData dataManager = (mmmData)app.getDataComponent();
        line = ((DraggableLabel)(dataManager.getSelectedNode())).getLine();
        oldColor = (Color) line.getFillLine();
        oldName = line.getName();
        newColor = newC;
        newName = newT;
    }
    
    @Override
    public void doTransaction() {
        for (int i = 0;i<=((mmmWorkspace)app.getWorkspaceComponent()).getLines().size()-1;i++){
                String s = (String)((mmmWorkspace)app.getWorkspaceComponent()).getLines().get(i);
                if (s.equals( oldName ) ){
                    ((mmmWorkspace)app.getWorkspaceComponent()).getLines().remove(s);
                }
            }
            ((mmmWorkspace)app.getWorkspaceComponent()).getLines().add(newName);
            (line.getBeginning()).setText(newName + " - END");
            (line.getEnding()).setText(newName + " - END");
            line.setName(newName);
            line.changeFillLine(newColor);
    }

    @Override
    public void undoTransaction() {
        for (int i = 0;i<=((mmmWorkspace)app.getWorkspaceComponent()).getLines().size()-1;i++){
                String s = (String)((mmmWorkspace)app.getWorkspaceComponent()).getLines().get(i);
                if (s.equals( newName ) ){
                    ((mmmWorkspace)app.getWorkspaceComponent()).getLines().remove(s);
                }
            }
            ((mmmWorkspace)app.getWorkspaceComponent()).getLines().add(oldName);
            (line.getBeginning()).setText(oldName + " - END");
            (line.getEnding()).setText(oldName + " - END");
            line.setName(oldName);
            line.changeFillLine(oldColor);
    }
    
}
