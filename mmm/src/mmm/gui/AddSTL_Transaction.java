/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import djf.AppTemplate;
import jtps.jTPS_Transaction;
import mmm.data.DraggableLineEnd;
import mmm.data.DraggableStation;

/**
 *
 * @author shawn
 */
public class AddSTL_Transaction implements jTPS_Transaction{
    
    DraggableStation station;
    DraggableLineEnd line;
    MapEditController mEC;
    
    public AddSTL_Transaction(DraggableStation selectedStation, DraggableLineEnd selectedLine, AppTemplate app){
        mEC = ((mmmWorkspace)app.getWorkspaceComponent()).getMapEditController();
        station = selectedStation;
        line = selectedLine;
    }
    
    @Override
    public void doTransaction() {
        mEC.processAddStationToLine(station, line);
    }

    @Override
    public void undoTransaction() {
        mEC.processRemoveStationFromLine(station, line);
    }
    
}
