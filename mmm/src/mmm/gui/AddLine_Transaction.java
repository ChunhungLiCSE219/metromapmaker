/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import djf.AppTemplate;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import jtps.jTPS_Transaction;
import mmm.data.DraggableLabel;
import mmm.data.DraggableLineEnd;
import mmm.data.mmmData;

/**
 *
 * @author shawn
 */
public class AddLine_Transaction implements jTPS_Transaction{
    
    DraggableLineEnd line;
    Color newColor;
    String name;
    mmmData dataManager;
    mmmWorkspace workspace;
    
    public AddLine_Transaction(Color c, String n, AppTemplate app){
        newColor = c;
        name = n;
        dataManager = (mmmData)app.getDataComponent();
        workspace = (mmmWorkspace)app.getWorkspaceComponent();
        line = new DraggableLineEnd(newColor,name,8);
        line.start((int)workspace.getCanvas().getWidth()/2, (int)workspace.getCanvas().getHeight()/2);
    }
    
    @Override
    public void doTransaction() {
        dataManager.addNode(line.getBeginning());
        dataManager.addNode(line.getEnding());
        dataManager.addNode((Line)line.getConnectors().get(0));
        workspace.getLines().add(line.toString());
        workspace.getLinesBox().setValue(line.toString());
        dataManager.highlightNode(line.getBeginning());
        dataManager.setSelectedNode(line.getBeginning());
        dataManager.getLines().add(line);
    }

    @Override
    public void undoTransaction() {
        for (int i = 0;i<=workspace.getLines().size()-1;i++){
            String s = (String)workspace.getLines().get(i);
            if (s.equals(line.getName())){
                workspace.getLines().remove(s);
                i--;
            }
        }
        dataManager.removeNode(line.getBeginning());
        dataManager.removeNode(line.getEnding());
        dataManager.removeNode((Line)line.getConnectors().get(0));
        dataManager.getLines().remove(line);
        dataManager.setSelectedNode(null);
    }
    
}
