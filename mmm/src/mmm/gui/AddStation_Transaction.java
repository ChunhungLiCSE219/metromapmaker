/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import djf.AppTemplate;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;
import mmm.data.DraggableStation;
import mmm.data.mmmData;

/**
 *
 * @author shawn
 */
public class AddStation_Transaction implements jTPS_Transaction{

    mmmData dataManager;
    mmmWorkspace workspace;
    DraggableStation station;
    
    public AddStation_Transaction(Color c, String n, AppTemplate app){
        dataManager = (mmmData)app.getDataComponent();
        workspace = (mmmWorkspace)app.getWorkspaceComponent();
        station = new DraggableStation(n);
        station.setFill(c);
        station.start((int)(workspace.getCanvas().getWidth()/2), (int)(workspace.getCanvas().getHeight()/2));
        station.setStationX((workspace.getCanvas().getWidth()/2));
        station.setStationY((workspace.getCanvas().getHeight()/2));
    }
    
    @Override
    public void doTransaction() {
        dataManager.addNode(station);
        dataManager.addNode(station.getName());
        workspace.getStations().add(station.toString());
        dataManager.getStations().add(station);
    }

    @Override
    public void undoTransaction() {
        for (int i = 0;i<=workspace.getStations().size()-1;i++){
            String s = workspace.getStations().get(i).toString();
            if (s.equals(station.toString())){
                workspace.getStations().remove(s);
            }
        }
        dataManager.getStations().remove(station);
        dataManager.removeNode(station);
        dataManager.removeNode(station.getName());
        dataManager.removeSelectedNode();
        dataManager.setSelectedNode(null);
    }
    
}
