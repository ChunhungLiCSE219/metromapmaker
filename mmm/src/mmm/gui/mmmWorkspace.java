/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import java.io.IOException;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import static mmm.mmmLanguageProperty.*;
import mmm.data.mmmData;
import mmm.data.mmmState;
import djf.ui.AppYesNoCancelDialogSingleton;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppGUI;
import djf.AppTemplate;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import static djf.settings.AppPropertyType.LOAD_ERROR_MESSAGE;
import static djf.settings.AppPropertyType.LOAD_ERROR_TITLE;
import static djf.settings.AppPropertyType.SAVE_WORK_TITLE;
import static djf.settings.AppPropertyType.WORK_FILE_EXT;
import static djf.settings.AppPropertyType.WORK_FILE_EXT_DESC;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import static djf.settings.AppStartupConstants.PATH_WORK;
import java.io.File;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import static mmm.css.mmmStyle.*;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Ellipse;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.util.StringConverter;
import jtps.jTPS;
import mmm.data.DraggableImageView;
import mmm.data.DraggableLabel;
import mmm.data.DraggableStation;
import properties_manager.PropertiesManager;
/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing maps.
 * @author shawn
 */
public class mmmWorkspace extends AppWorkspaceComponent{

    AppTemplate app;
    AppGUI gui;
    jTPS stack;
    int stackSize;
    int stackIndex;
    String fileName;
    
    Button saveAs;
    Button export;
    Button undo;
    Button redo;
    Button about;
    
    VBox editToolBar;
    
    VBox metroLinesVBox;
    HBox mLHBox1;
    Text metroLinesLabel;
    ComboBox lines;
    Ellipse lineDisplay;
    Text lineColorDisplay;
    StackPane lineDisplayALL;
    HBox mLHBox2;
    Button addLine;
    Button removeLine;
    Button addStationToLine;
    Button removeStationFromLine;
    Button listLines;
    HBox mLHBox3;
    Slider lineThickSlider;
    
    VBox metroStationsVBox;
    HBox mSHBox1;
    Text metroStationsLabel;
    ComboBox stations;
    Ellipse stationDisplay;
    Text stationColorDisplay;
    StackPane stationDisplayALL;
    HBox mSHBox2;
    Button addStation;
    Button removeStation;
    Button snap;
    Button moveLabel;
    Button rotateLabel;
    HBox mSHBox3;
    Slider radiusSlider;
    
    HBox routeHBox;
    VBox rVBox1;
    ComboBox station1;
    ComboBox station2;
    VBox rVBox2;
    Button route;
    
    VBox decorVBox;
    HBox decorHBox1;
    Text decorLabel;
    Ellipse bgColor;
    Text bgColorDisplay;
    StackPane bgColorDisplayALL;
    HBox decorHBox2;
    Button imageBackground;
    Button addImage;
    Button addLabel;
    Button removeElement;
    
    VBox fontVBox;
    HBox fHBox1;
    Text fontLabel;
    Ellipse fontColor;
    Text fontColorLabel;
    StackPane fontColorDisplayALL;
    HBox fHBox2;
    Button bolden;
    Button italicize;
    ComboBox fontSize;
    ComboBox fontFamily;
    VBox navigationVBox;
    
    VBox navigationHBox;
    HBox nHBox1;
    Text navigationLabel;
    ToggleButton showGrid; 
    HBox nHBox2;
    Button zoomIn;
    Button zoomOut;
    Button shrink;
    Button grow;
    
    Pane canvas;
    CanvasController canvasController;
    MapEditController mapEditController;
    
    AppMessageDialogSingleton messageDialog;
    AppYesNoCancelDialogSingleton yesNoCancelDialog;
    
    Text debugText;
    
     /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public mmmWorkspace(AppTemplate initApp) {
	// KEEP THIS FOR LATER
        System.out.println("mmmWorkspace has been instantiated");
	app = initApp;
	// KEEP THE GUI FOR LATER
	gui = app.getGUI();
        
        // LAYOUT THE APP
        System.out.println("good up until now");
        initLayout();
        System.out.println("initLayout has been called");
        // HOOK UP THE CONTROLLERS
        initControllers();
        System.out.println("initControllers has been called");
        // AND INIT THE STYLE FOR THE WORKSPACE
        initStyle();    
        System.out.println("initStyle has been called");
        stack = new jTPS();
    }
    
    /**
     * Note that this is for displaying text during development.
     */
    public void setDebugText(String text) {
	debugText.setText(text);
    }
    
    // ACCESSOR METHODS FOR COMPONENTS THAT EVENT HANDLERS
    // MAY NEED TO UPDATE OR ACCESS DATA FROM
    public Slider getRadiusSlider(){
        return radiusSlider;
    }
    
    public Slider getLineThickSlider(){
        return lineThickSlider;
    }
    
    public Pane getCanvas(){
        return canvas;
    }
    
    @Override
    public void resetWorkspace() {
        stack = new jTPS();
        stackSize = 0;
        stackIndex = 0;
    }

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        mmmData dataManager = (mmmData)dataComponent;
        if (dataManager.isInState(mmmState.SELECTING_SHAPE)){
            
        } else if (dataManager.isInState(mmmState.DRAGGING_NOTHING)){
            updateFontEditToolBar(false);
            updateLineEditToolBar(false);
            updateStationEditToolBar(false);
        } else if (dataManager.isInState(mmmState.DRAGGING_SHAPE)){
            if (dataManager.getSelectedNode() instanceof DraggableLabel && ((DraggableLabel)(dataManager.getSelectedNode())).isLineEnd()){
                updateLineEditToolBar(true);
                updateStationEditToolBar(false);
                updateFontEditToolBar(false);
                updateDecorEditToolBar(false);
            } else if (dataManager.getSelectedNode() instanceof DraggableStation){
                updateStationEditToolBar(true);
                updateLineEditToolBar(false);
                updateFontEditToolBar(true);
                updateDecorEditToolBar(false);
            } else if (dataManager.getSelectedNode() instanceof DraggableImageView) {
                updateFontEditToolBar(false);
                updateStationEditToolBar(false);
                updateLineEditToolBar(false);
                updateDecorEditToolBar(true);
            } else if (dataManager.getSelectedNode() instanceof DraggableLabel) {
                updateFontEditToolBar(true);
                updateStationEditToolBar(false);
                updateLineEditToolBar(false);
                updateDecorEditToolBar(true);    
            }
            
        } 
    }
    // HELPER SETUP METHOD
    private void initLayout() {
        // THIS WILL GO IN THE TOP OF THE WORKSPACE
        FlowPane transactionToolBar = new FlowPane();
        System.out.println("Right before first call");
        ((Button)app.getGUI().getFileToolbar().getChildren().get(0)).addEventHandler(ActionEvent.ACTION,(e)->{
            TextInputDialog fileDialog = new TextInputDialog("NewMap");
            fileDialog.setTitle("New Map Name");
            fileDialog.setHeaderText("");
            fileDialog.setContentText("Enter Map Name:");
            Optional<String> result = fileDialog.showAndWait();
            fileName = result.toString().substring(9, result.toString().length()-1);
            new File("./export/" + fileName).mkdirs();
            Path p = Paths.get("./export/" + fileName + "/" + fileName + " Metro.json");
            try {
                Files.createFile(p);
            } catch (IOException ex) {
                System.err.format("createFile error: %s%n", ex);
            }
        });
        saveAs = app.getGUI().initChildButton(app.getGUI().getFileToolbar(), SAVEAS_ICON.toString(), SAVEAS_TOOLTIP.toString(), false);
        saveAs.setOnAction(e->{
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            try {
                FileChooser fc = new FileChooser();
                fc.setInitialDirectory(new File(PATH_WORK));
                fc.setTitle(props.getProperty(SAVE_WORK_TITLE));
                fc.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter(props.getProperty(WORK_FILE_EXT_DESC), props.getProperty(WORK_FILE_EXT)));

                File selectedFile = fc.showSaveDialog(app.getGUI().getWindow());
                if (selectedFile != null) {
		    app.getFileComponent().saveData(app.getDataComponent(), selectedFile.getPath());
                }
            }catch (IOException ioe) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(LOAD_ERROR_TITLE), props.getProperty(LOAD_ERROR_MESSAGE));
            }
        });
        export = app.getGUI().initChildButton(app.getGUI().getFileToolbar(), EXPORT_ICON.toString(), EXPORT_TOOLTIP.toString(), false);
        System.out.println("Right after first call");
        transactionToolBar.getStyleClass().add(CLASS_BORDERED_PANE);
        undo = app.getGUI().initChildButton(transactionToolBar, UNDO_ICON.toString(), UNDO_TOOLTIP.toString(), true);
        redo = app.getGUI().initChildButton(transactionToolBar, REDO_ICON.toString(), REDO_TOOLTIP.toString(), true);
        app.getGUI().getTopToolbarPane().getChildren().add(transactionToolBar);
        FlowPane otherToolBar = new FlowPane();
        otherToolBar.getStyleClass().add(CLASS_BORDERED_PANE);
        about = app.getGUI().initChildButton(otherToolBar, ABOUT_ICON.toString(), ABOUT_TOOLTIP.toString(), false);
        app.getGUI().getTopToolbarPane().getChildren().add(otherToolBar);
        HBox LogoToolBar = new HBox();
        LogoToolBar.setMinWidth(120);
        Image logo = new Image("file:./images/MMMLogo.png");
        ImageView logoView = new ImageView(logo);
        logoView.setFitWidth(95.0);
        logoView.setFitHeight(100.0);
        LogoToolBar.getChildren().add(logoView);
        LogoToolBar.setAlignment(Pos.CENTER_LEFT);
        app.getGUI().getTopToolbarPane().getChildren().add(0,LogoToolBar);
        // THIS WILL GO IN THE LEFT SIDE OF THE WORKSPACE
        editToolBar = new VBox();
        metroLinesVBox = new VBox();
        mLHBox1 = new HBox();
        metroLinesLabel = new Text("Metro Lines");
        metroLinesLabel.setFont(Font.font("Arial,Helvetica,sans-serif",FontWeight.BOLD,FontPosture.REGULAR,18));
        metroLinesLabel.setFill(Color.WHITE);
        lines = new ComboBox();
        lines.setPrefWidth(230.0);
        lineDisplay = new Ellipse(20.0,20.0);
        lineDisplay.setFill(Color.WHITE);
        lineColorDisplay = new Text("#"+lineDisplay.getFill().toString().substring(2,8));
        lineColorDisplay.setFont(Font.font("Arial,Helvetica,sans-serif", 11));
        lineDisplayALL = new StackPane();
        lineDisplayALL.getChildren().addAll(lineDisplay,lineColorDisplay);
        mLHBox1.getChildren().add(metroLinesLabel);
        mLHBox1.getChildren().add(lines);
        mLHBox1.getChildren().add(lineDisplayALL);
        mLHBox1.setAlignment(Pos.CENTER_LEFT);
        mLHBox2 = new HBox();
        addLine = new Button("+");
        Tooltip addLineTT = new Tooltip("add a line");
        addLine.setTooltip(addLineTT);
        removeLine = new Button("-");
        removeLine.setDisable(true);
        Tooltip removeLineTT = new Tooltip("remove current line");
        removeLine.setTooltip(removeLineTT);
        addStationToLine = new Button("Add\nStation");
        addStationToLine.setDisable(true);
        Tooltip addStationToLineTT = new Tooltip("add station to line");
        addStationToLine.setTooltip(addStationToLineTT);
        removeStationFromLine = new Button("Remove\nStation");
        removeStationFromLine.setDisable(true);
        Tooltip removeStationFromLineTT = new Tooltip("remove Station from Line");
        removeStationFromLine.setTooltip(removeStationFromLineTT);
        mLHBox2.getChildren().add(addLine);
        mLHBox2.getChildren().add(removeLine);
        mLHBox2.getChildren().add(addStationToLine);
        mLHBox2.getChildren().add(removeStationFromLine);
        mLHBox2.setAlignment(Pos.CENTER_LEFT);
        listLines = app.getGUI().initChildButton(mLHBox2, LISTLINES_ICON.toString(), LISTLINES_TOOLTIP.toString(), true);
        mLHBox3 = new HBox();
        lineThickSlider = new Slider(2,20,1);
        Tooltip lineThickTT = new Tooltip("line thickness");
        lineThickSlider.setTooltip(lineThickTT);
        lineThickSlider.setPrefWidth(375.0);
        mLHBox3.getChildren().add(lineThickSlider);
        metroLinesVBox.getChildren().add(mLHBox1);
        metroLinesVBox.getChildren().add(mLHBox2);
        metroLinesVBox.getChildren().add(mLHBox3);
        metroStationsVBox = new VBox();
        mSHBox1 = new HBox();
        metroStationsLabel = new Text("Metro Stations");
        metroStationsLabel.setFont(Font.font("Arial,Helvetica,sans-serif",FontWeight.BOLD,FontPosture.REGULAR,18));
        metroStationsLabel.setFill(Color.WHITE);
        stations = new ComboBox();
        stations.setPrefWidth(210.0);
        stationDisplay = new Ellipse(20.0,20.0);
        stationDisplay.setFill(Color.WHITE);
        stationColorDisplay = new Text("#"+stationDisplay.getFill().toString().substring(2, 8));
        stationColorDisplay.setFont(Font.font("Arial,Helvetica,sans-serif",11));
        stationDisplayALL = new StackPane();
        stationDisplayALL.getChildren().addAll(stationDisplay, stationColorDisplay);
        mSHBox1.getChildren().add(metroStationsLabel);
        mSHBox1.getChildren().add(stations);
        mSHBox1.getChildren().add(stationDisplayALL);
        mSHBox1.setAlignment(Pos.CENTER_LEFT);
        mSHBox2 = new HBox();
        addStation = new Button("+");
        Tooltip addStationTT = new Tooltip("add a station");
        addStation.setTooltip(addStationTT);
        removeStation = new Button("-");
        removeStation.setDisable(true);
        Tooltip removeStationTT = new Tooltip("remove current station");
        removeStation.setTooltip(removeStationTT);
        snap = new Button("Snap");
        snap.setDisable(true);
        Tooltip snapTT = new Tooltip("Snap to grid");
        snap.setTooltip(snapTT);
        moveLabel = new Button("Move\nLabel");
        moveLabel.setDisable(true);
        Tooltip moveLabelTT = new Tooltip("Move Label");
        moveLabel.setTooltip(moveLabelTT);
        rotateLabel = new Button("Rotate\nLabel"); //replace with icon
        rotateLabel.setDisable(true);
        Tooltip rotateLabelTT = new Tooltip("Rotate Label");
        rotateLabel.setTooltip(rotateLabelTT);
        mSHBox2.getChildren().add(addStation);
        mSHBox2.getChildren().add(removeStation);
        mSHBox2.getChildren().add(snap);
        mSHBox2.getChildren().add(moveLabel);
        mSHBox2.getChildren().add(rotateLabel);
        mSHBox2.setAlignment(Pos.CENTER_LEFT);
        mSHBox3 = new HBox();
        radiusSlider = new Slider(5,25,1);
        Tooltip radiusSliderTT = new Tooltip("Set Station Size");
        radiusSlider.setTooltip(radiusSliderTT);
        radiusSlider.setPrefWidth(375.0);
        mSHBox3.getChildren().add(radiusSlider);
        metroStationsVBox.getChildren().add(mSHBox1);
        metroStationsVBox.getChildren().add(mSHBox2);
        metroStationsVBox.getChildren().add(mSHBox3);
        routeHBox = new HBox();
        rVBox1 = new VBox();
        station1 = new ComboBox();
        station1.itemsProperty().bind(stations.itemsProperty());
        station1.setPrefWidth(275);
        station2 = new ComboBox();
        station2.itemsProperty().bind(stations.itemsProperty());
        station2.setPrefWidth(275);
        rVBox1.getChildren().addAll(station1,station2);
        rVBox2 = new VBox();
        route = app.getGUI().initChildButton(rVBox2, ROUTE_ICON.toString(), ROUTE_TOOLTIP.toString(), true);
        route.setPrefWidth(85);
        route.prefHeightProperty().bind(rVBox2.heightProperty());
        rVBox2.setAlignment(Pos.CENTER_LEFT);
        routeHBox.getChildren().add(rVBox1);
        routeHBox.getChildren().add(rVBox2);
    
        decorVBox = new VBox();
        decorHBox1 = new HBox();
        decorLabel = new Text("Decor");
        decorLabel.setFont(Font.font("Arial,Helvetica,sans-serif",FontWeight.BOLD,FontPosture.REGULAR,18));
        decorLabel.setFill(Color.WHITE);
        bgColor = new Ellipse(20.0,20.0);
        bgColor.setFill(Color.WHITE);
        bgColorDisplayALL = new StackPane();
        bgColorDisplay = new Text("#" + bgColor.getFill().toString().substring(2, 8));
        bgColorDisplay.setFont(Font.font("Arial,Helvetica,sans-serif",11));
        bgColorDisplayALL.getChildren().addAll(bgColor,bgColorDisplay);
        decorHBox1.getChildren().add(decorLabel);
        decorHBox1.getChildren().add(bgColorDisplayALL);
        decorHBox1.setSpacing(280);
        decorHBox1.setAlignment(Pos.CENTER_LEFT);
        decorHBox2 = new HBox();
        imageBackground = new Button("Set Image\nBackground");
        Tooltip imageBackgroundTT = new Tooltip("Set Image Background");
        imageBackground.setTooltip(imageBackgroundTT);
        addImage = new Button("Add\nImage");
        Tooltip addImageTT = new Tooltip("Add Image Overlay");
        addImage.setTooltip(addImageTT);
        addLabel = new Button("Add\nLabel");
        Tooltip addLabelTT = new Tooltip("Add Text Element");
        addLabel.setTooltip(addLabelTT);
        removeElement = new Button("Remove\nElement");
        removeElement.setDisable(true);
        Tooltip removeElementTT = new Tooltip("Remove Decoration Element");
        removeElement.setTooltip(removeElementTT);
        decorHBox2.getChildren().add(imageBackground);
        decorHBox2.getChildren().add(addImage);
        decorHBox2.getChildren().add(addLabel);
        decorHBox2.getChildren().add(removeElement);
        decorHBox2.setAlignment(Pos.CENTER_LEFT);
        decorVBox.getChildren().add(decorHBox1);
        decorVBox.getChildren().add(decorHBox2);
        fontVBox = new VBox();
        fHBox1 = new HBox();
        fontLabel = new Text("Font");
        fontLabel.setFont(Font.font("Arial,Helvetica,sans-serif",FontWeight.BOLD,FontPosture.REGULAR,18));
        fontLabel.setFill(Color.WHITE);
        fontColor = new Ellipse(20.0,20.0);
        fontColor.setFill(Color.BLACK);
        fontColorLabel = new Text("#"+fontColor.getFill().toString().substring(2,8));
        fontColorLabel.setFont(Font.font("Arial,Helvetica,sans-serif",11));
        fontColorLabel.setFill(Color.WHITE);
        fontColorLabel.setFill(Color.WHITE);
        fontColorDisplayALL = new StackPane();
        fontColorDisplayALL.getChildren().addAll(fontColor,fontColorLabel);
        fHBox1.getChildren().add(fontLabel);
        fHBox1.getChildren().add(fontColorDisplayALL);
        fHBox1.setAlignment(Pos.CENTER_LEFT);
        fHBox1.setSpacing(290);
        fHBox2 = new HBox();
        bolden = new Button("B"); //bolden
        bolden.setDisable(true);
        Tooltip boldenTT = new Tooltip("Bolden selected text");
        bolden.setTooltip(boldenTT);
        italicize = new Button("I"); //italicize
        italicize.setDisable(true);
        Tooltip italicizeTT = new Tooltip("Italicize selected text");
        italicize.setTooltip(italicizeTT);
        fontSize = new ComboBox();
        fontSize.getItems().addAll(
                6,8,9,10,11,12,13,14,16,18,
                20,22,24,26,28,30,32,34,36,
                40,44,48,52,56,60,64,
                72,80,88,96,108,120
        );
        Tooltip fontSizeTT = new Tooltip("Set font size of selected text");
        fontSize.setTooltip(fontSizeTT);
        fontSize.setPrefWidth(140.0);
        fontFamily = new ComboBox();
        fontFamily.getItems().addAll(
                "Arial,Helvetica,sans-serif",
                "Verdana",
                "Times New Roman",
                "Century Gothic",
                "Courier",
                "Courier New",
                "Lucida Sans Unicode"
        );
        Tooltip fontFamilyTT = new Tooltip("Set font family of selected text");
        fontFamily.setTooltip(fontFamilyTT);
        fontFamily.setPrefWidth(140.0);
        fHBox2.getChildren().add(bolden);
        fHBox2.getChildren().add(italicize);
        fHBox2.getChildren().add(fontSize);
        fHBox2.getChildren().add(fontFamily);
        fHBox2.setAlignment(Pos.CENTER_LEFT);
        fontVBox.getChildren().add(fHBox1);
        fontVBox.getChildren().add(fHBox2);
        
        navigationVBox = new VBox();    
        nHBox1 = new HBox();
        navigationLabel = new Text("Navigation");
        navigationLabel.setFont(Font.font("Arial,Helvetica,sans-serif",FontWeight.BOLD,FontPosture.REGULAR,18));
        navigationLabel.setFill(Color.WHITE);
        showGrid = new ToggleButton("Show Grid");
        showGrid.setStyle("-fx-text-fill: white;-fx-font-size: 18;");
        nHBox1.getChildren().add(navigationLabel);
        nHBox1.getChildren().add(showGrid);
        nHBox1.setAlignment(Pos.CENTER_LEFT);
        nHBox1.setSpacing(165);
        nHBox2 = new HBox();
        zoomIn = app.getGUI().initChildButton(nHBox2, ZOOMIN_ICON.toString(), ZOOMIN_TOOLTIP.toString(), false);
        zoomOut = app.getGUI().initChildButton(nHBox2, ZOOMOUT_ICON.toString(), ZOOMOUT_TOOLTIP.toString(), false);
        shrink = app.getGUI().initChildButton(nHBox2, SHRINK_ICON.toString(), SHRINK_TOOLTIP.toString(), false);
        grow = app.getGUI().initChildButton(nHBox2, GROW_ICON.toString(), GROW_TOOLTIP.toString(), false);
        navigationVBox.getChildren().add(nHBox1);
        navigationVBox.getChildren().add(nHBox2);
        
        editToolBar.getChildren().add(metroLinesVBox);
        editToolBar.getChildren().add(metroStationsVBox);
        editToolBar.getChildren().add(routeHBox);
        editToolBar.getChildren().add(decorVBox);
        editToolBar.getChildren().add(fontVBox);
        editToolBar.getChildren().add(navigationVBox);
        canvas = new Pane();
        debugText = new Text();
        canvas.getChildren().add(debugText);
        
        canvas.setStyle("-fx-background-color: #ffffff;");
        debugText.setX(100);
	debugText.setY(100);
        editToolBar.prefHeightProperty().bind(app.getGUI().getAppPane().heightProperty());
        mmmData data = (mmmData)app.getDataComponent();
        data.setFileName(fileName);
	data.setNodes(canvas.getChildren());

	// AND NOW SETUP THE WORKSPACE
	workspace = new BorderPane();
	((BorderPane)workspace).setLeft(editToolBar);
	((BorderPane)workspace).setCenter(canvas);
    }

    private void initControllers() {
        mapEditController = new MapEditController(app);
        canvasController = new CanvasController(app);
        canvas.setOnMousePressed(e->{
            canvasController.processCanvasMousePress(e.getX(), e.getY());
        });
        canvas.setOnMouseDragged(e->{
            canvasController.processCanvasMouseDragged(e.getX(), e.getY());
        });
        canvas.setOnMouseReleased(e->{
            canvasController.processCanvasMouseRelease(e.getX(), e.getY());
        });
        about.setOnAction(e->{
            processAbout();
        });
        export.setOnAction(e->{
            mapEditController.processSnapShot();
            try {
                app.getFileComponent().exportData((mmmData)app.getDataComponent(), "./export/" + ((mmmData)app.getDataComponent()).getFileName() + "/" + ((mmmData)app.getDataComponent()).getFileName() + " Metro");
            } catch (IOException ex) {
                Logger.getLogger(mmmWorkspace.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        addLine.setOnAction(e->{
            canvasController.processAddLine();
            app.getGUI().updateToolbarControls(false);
        });
        removeLine.setOnAction(e->{
            mapEditController.processRemoveLine();
            app.getGUI().updateToolbarControls(false);
        });
        addStationToLine.setOnAction(e->{
            mapEditController.processChangeMode(mmmState.ADD_STATION);
            app.getGUI().updateToolbarControls(false);
        });
        removeStationFromLine.setOnAction(e->{
            mapEditController.processChangeMode(mmmState.REMOVE_STATION);
            app.getGUI().updateToolbarControls(false);
        });
        listLines.setOnAction(e->{
            mapEditController.processListStation();
        });
        addStation.setOnAction(e->{
            canvasController.processAddStation();
            app.getGUI().updateToolbarControls(false);
        });
        removeStation.setOnAction(e->{
            mapEditController.processRemoveStation();
            app.getGUI().updateToolbarControls(false);
        });
        snap.setOnAction(e->{
            mapEditController.processSnapToGrid();
            app.getGUI().updateToolbarControls(false);
        });
        moveLabel.setOnAction(e->{
            mapEditController.processMoveStationLabel();
            app.getGUI().updateToolbarControls(false);
        });
        rotateLabel.setOnAction(e->{
            mapEditController.processRotateStationLabel();
            app.getGUI().updateToolbarControls(false);
        });
        station1.setOnAction(e->{
            if (station1.getValue() != null && station2.getValue() != null) {
                updateRouteEditToolBar(true);
            } else {
                updateRouteEditToolBar(false);
            }
        });
        station2.setOnAction(e->{
            if (station1.getValue() != null && station2.getValue() != null) {
                updateRouteEditToolBar(true);
            } else {
                updateRouteEditToolBar(false);
            }
        });
        route.setOnAction(e->{
            mapEditController.processFindRoute();
        });
        imageBackground.setOnAction(e->{
            mapEditController.processSelectBackgroundImage();
            app.getGUI().updateToolbarControls(false);
        });
        addImage.setOnAction(e->{
            mapEditController.processEmbedImage();
            app.getGUI().updateToolbarControls(false);
        });
        addLabel.setOnAction(e->{
            canvasController.processAddLabel();
            app.getGUI().updateToolbarControls(false);
        });
        removeElement.setOnAction(e->{
            mapEditController.processRemoveSelected();
            app.getGUI().updateToolbarControls(false);
        });
        bolden.setOnAction(e->{
            canvasController.processModifyFont(-1000);
            app.getGUI().updateToolbarControls(false);
        });
        italicize.setOnAction(e->{
            canvasController.processModifyFont(-2000);
            app.getGUI().updateToolbarControls(false);
        });
        zoomIn.setOnAction(e->{
            canvasController.processZoom(true);
        });
        zoomOut.setOnAction(e->{
            canvasController.processZoom(false);
        });
        shrink.setOnAction(e->{
            canvasController.processResize(false);
        });
        grow.setOnAction(e->{
            canvasController.processResize(true);
        });
        radiusSlider.setOnMouseReleased(e->{
            mapEditController.processRadiusSize(radiusSlider.getValue());
            app.getGUI().updateToolbarControls(false);
        });
        lineThickSlider.setOnMouseReleased(e->{
            mapEditController.processSelectOutlineThickness(lineThickSlider.getValue());
            app.getGUI().updateToolbarControls(false);
        });
        lines.setOnAction(e->{
            mapEditController.processSelectLine();
        });
        stations.setOnAction(e->{
            mapEditController.processSelectStation();
        });
        fontSize.setOnAction(e->{
            System.out.println("Font Size: " + (int)fontSize.getValue());
            canvasController.processModifyFont((int) fontSize.getValue());
            app.getGUI().updateToolbarControls(false);
        });
        fontFamily.setOnAction(e->{
            canvasController.processModifyFont(0);
            app.getGUI().updateToolbarControls(false);
        });
        showGrid.setOnAction(e->{
            mapEditController.processShowGrid();
        });
        redo.setOnAction(e->{
            handleRedoRequest();
            app.getGUI().updateToolbarControls(false);
        });
        undo.setOnAction(e->{
            handleUndoRequest();
            app.getGUI().updateToolbarControls(false);
        });
        lineDisplayALL.setOnMouseClicked(e->{
            if (e.getClickCount() == 2 && lines.getValue() != null){
                 canvasController.processEditLine();
            } 
        });
        stationDisplayALL.setOnMouseClicked(e->{
            if (e.getClickCount() == 2 && stations.getValue() != null){
                 canvasController.processEditStation();
            } 
        });
        bgColorDisplayALL.setOnMouseClicked(e->{
            if (e.getClickCount() == 2){
                mapEditController.processSelectBackgroundColor();
            }
        });
        fontColorDisplayALL.setOnMouseClicked(e->{
            if (e.getClickCount() == 2 && 
                    ((((mmmData)(app.getDataComponent())).getSelectedNode() instanceof DraggableLabel) && !((DraggableLabel)((mmmData)(app.getDataComponent())).getSelectedNode()).isLineEnd()
                    || (((mmmData)(app.getDataComponent())).getSelectedNode() instanceof Text) && !(((mmmData)(app.getDataComponent())).getSelectedNode() instanceof DraggableLabel))){
                mapEditController.processSelectFontColor();
            }
        });
    }

    private void initStyle() {
        canvas.getStyleClass().add(CLASS_RENDER_CANVAS);
        editToolBar.getStyleClass().add(CLASS_EDIT_TOOLBAR);
        metroLinesVBox.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        mLHBox1.getStyleClass().add(CLASS_ROW_HORIZONTAL_COMPONENT);
        lines.getStyleClass().add(CLASS_COMBOBOX);
        mLHBox2.getStyleClass().add(CLASS_ROW_HORIZONTAL_COMPONENT);
        addLine.getStyleClass().add(CLASS_BUTTON);
        removeLine.getStyleClass().add(CLASS_BUTTON);
        addStationToLine.getStyleClass().add(CLASS_BUTTON);
        removeStationFromLine.getStyleClass().add(CLASS_BUTTON);
        listLines.getStyleClass().add(CLASS_BUTTON);
        mLHBox3.getStyleClass().add(CLASS_ROW_HORIZONTAL_COMPONENT);
        lineThickSlider.getStyleClass().add(CLASS_SLIDER);
        metroStationsVBox.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        mSHBox1.getStyleClass().add(CLASS_ROW_HORIZONTAL_COMPONENT);
        stations.getStyleClass().add(CLASS_COMBOBOX);
        mSHBox2.getStyleClass().add(CLASS_ROW_HORIZONTAL_COMPONENT);
        mSHBox3.getStyleClass().add(CLASS_ROW_HORIZONTAL_COMPONENT);
        radiusSlider.getStyleClass().add(CLASS_SLIDER);
        routeHBox.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        rVBox1.getStyleClass().add(CLASS_ROW_VERTICAL_COMPONENT);
        station1.getStyleClass().add(CLASS_COMBOBOX);
        station2.getStyleClass().add(CLASS_COMBOBOX);
        rVBox2.getStyleClass().add(CLASS_ROW_VERTICAL_COMPONENT);
        decorVBox.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        decorHBox1.getStyleClass().add(CLASS_ROW_HORIZONTAL_COMPONENT);
        decorHBox2.getStyleClass().add(CLASS_ROW_HORIZONTAL_COMPONENT);
        fontVBox.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        fHBox1.getStyleClass().add(CLASS_ROW_HORIZONTAL_COMPONENT);
        fHBox2.getStyleClass().add(CLASS_ROW_HORIZONTAL_COMPONENT);
        fontSize.getStyleClass().add(CLASS_COMBOBOX);
        fontFamily.getStyleClass().add(CLASS_COMBOBOX);
        navigationVBox.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        nHBox1.getStyleClass().add(CLASS_ROW_HORIZONTAL_COMPONENT);
        nHBox2.getStyleClass().add(CLASS_ROW_HORIZONTAL_COMPONENT);
    }

    private void processAbout() {
        Alert alert = new Alert(AlertType.INFORMATION);
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        alert.setTitle("About");
        alert.setHeaderText("Metro Map Maker");
        alert.setContentText("Product Name: \tMetro Map Maker\nMade By: \t\t\tDebugging Enterprise\nFrameworks Used: \tDesktopJavaFramework\nDevelopers: \t\tShawn Li\nYear of Work: \t\t2017");
        alert.showAndWait();
    }
    private void handleUndoRequest(){
        stack.undoTransaction();
        stackIndex -= 1 ;
        if (stackIndex == 0){
            undo.setDisable(true);
        }
        redo.setDisable(false);
    }
    private void handleRedoRequest(){
        stack.doTransaction();
        stackIndex += 1;
        if (stackIndex >= stackSize - 1){
            redo.setDisable(true);
        } 
        undo.setDisable(false);
    }
    public void setFileName(String file){
        fileName = file;
    }
    public String getFileName(){
        return fileName;
    }
    public ObservableList getStations(){
        return stations.getItems();
    }
    public ComboBox getStationsBox(){
        return stations;
    }
    public ComboBox getFontFamilyBox(){
        return fontFamily;
    }
    public ComboBox getFontSizeBox(){
        return fontSize;
    }
    public ObservableList getLines(){
        return lines.getItems();
    }
    public ComboBox getLinesBox(){
        return lines;
    }
    public ComboBox getStationBox1(){
        return station1;
    }
    public ComboBox getStationBox2() {
        return station2;
    }
    public MapEditController getMapEditController(){
        return mapEditController;
    } 
    public CanvasController getCanvasController(){
        return canvasController;
    }
    public void updateLineEditToolBar(boolean lineExists){
        removeLine.setDisable(!lineExists);
        addStationToLine.setDisable(!lineExists);
        removeStationFromLine.setDisable(!lineExists);
        listLines.setDisable(!lineExists);
        if (!lineExists) {
            lines.setValue(null);
        }
    }
    public void updateStationEditToolBar(boolean stationExists){
        removeStation.setDisable(!stationExists);
        snap.setDisable(!stationExists);
        moveLabel.setDisable(!stationExists);
        rotateLabel.setDisable(!stationExists);
        if (!stationExists) {
            stations.setValue(null);
        }
    }
    public void updateRouteEditToolBar(boolean bothStationsExists){
        route.setDisable(!bothStationsExists);
    }
    public void updateDecorEditToolBar(boolean remove){
        removeElement.setDisable(!remove);
    }
    public void updateFontEditToolBar(boolean textExists){
        bolden.setDisable(!textExists);
        italicize.setDisable(!textExists);
        if (!textExists) {
            fontFamily.setValue(null);
            fontSize.setValue(null);
        }
    }
    
    
    public jTPS getStack(){
        return stack;
    }
    
    public boolean isStackEmpty(){
        String s = stack.toString();
        String c = s.substring(26,27);
        int i = Integer.getInteger(c.substring(26, 27));
        return i == 0;
    }
    
    public void addStack(){
        stackIndex += 1;
        stackSize = stackIndex + 1;
        undo.setDisable(false);
        redo.setDisable(true);
    }
}
