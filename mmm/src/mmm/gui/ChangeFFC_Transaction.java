/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import djf.AppTemplate;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import jtps.jTPS_Transaction;
import mmm.data.mmmData;

/**
 *
 * @author shawn
 */
public class ChangeFFC_Transaction implements jTPS_Transaction{

    Text text;
    Color newColor;
    Color oldColor;
    
    public ChangeFFC_Transaction(Color newC, AppTemplate app){
        mmmData dataManager = (mmmData)app.getDataComponent();
        text = (Text)dataManager.getSelectedNode();
        oldColor = (Color) text.getFill();
        newColor = newC;
    }
    
    @Override
    public void doTransaction() {
        text.setFill(newColor);
    }

    @Override
    public void undoTransaction() {
        text.setFill(oldColor);
    }
    
}
