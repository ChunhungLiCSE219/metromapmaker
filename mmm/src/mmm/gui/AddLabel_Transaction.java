/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import djf.AppTemplate;
import jtps.jTPS_Transaction;
import mmm.data.DraggableLabel;
import mmm.data.mmmData;

/**
 *
 * @author shawn
 */
public class AddLabel_Transaction implements jTPS_Transaction{
    
    DraggableLabel label;
    mmmWorkspace workspace;
    mmmData dataManager;
    
    public AddLabel_Transaction(String name,AppTemplate app){
        label = new DraggableLabel(name);
        workspace = (mmmWorkspace)app.getWorkspaceComponent();
        dataManager = (mmmData)app.getDataComponent();
    }
    
    @Override
    public void doTransaction() {
        label.start((int)workspace.getCanvas().getWidth()/2,(int)workspace.getCanvas().getHeight()/2);
        label.setX((int)workspace.getCanvas().getWidth()/2);
        label.setY((int)workspace.getCanvas().getHeight()/2);
        dataManager.addNode(label);
    }

    @Override
    public void undoTransaction() {
        dataManager.getNodes().remove(label);
        dataManager.setSelectedNode(null);
    }
    
}
