/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import djf.AppTemplate;
import javafx.scene.image.Image;
import jtps.jTPS_Transaction;
import mmm.data.DraggableImageView;
import mmm.data.mmmData;

/**
 *
 * @author shawn
 */
public class AddImageOverLay_Transaction implements jTPS_Transaction{

    DraggableImageView imageView;
    mmmWorkspace workspace;
    mmmData dataManager;
    
    public AddImageOverLay_Transaction(Image image, String path, AppTemplate app){
        imageView = new DraggableImageView(image);
        imageView.setPath(path);
        workspace = (mmmWorkspace)app.getWorkspaceComponent();
        dataManager = (mmmData)app.getDataComponent();
    }
    
    @Override
    public void doTransaction() {
        imageView.setX(workspace.getCanvas().getWidth()/2 - imageView.getWidth()/2);
        imageView.setY(workspace.getCanvas().getHeight()/2 - imageView.getHeight()/2);
        dataManager.addNode(imageView);
    }

    @Override
    public void undoTransaction() {
        dataManager.getNodes().remove(imageView);
        dataManager.setSelectedNode(null);
    }
    
}
